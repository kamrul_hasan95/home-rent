package star.lut.com.homerentapp.appModule.settings.view;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.Switch;

import butterknife.BindView;
import butterknife.OnClick;
import star.lut.com.homerentapp.R;
import star.lut.com.homerentapp.base.BaseFragment;
import star.lut.com.homerentapp.db.PreferenceManager;

/**
 * Created by kamrulhasan on 22/10/18.
 */
public class SettingsFragment extends BaseFragment {
    private static final String ARG_TITLE = "title";
    private String title;

    @BindView(R.id.stcNotification) public Switch stcNotification;
    @BindView(R.id.btnChangeLanguage) public Button btnCangelanguage;


    @Override
    public int getLayoutId() {
        return R.layout.fragment_settings;
    }

    public SettingsFragment() {
    }

    public SettingsFragment instanceOf(){
        SettingsFragment sortingFragment = new SettingsFragment();
        Bundle args = new Bundle();
        args.putString(ARG_TITLE, title);
        sortingFragment.setArguments(args);
        return sortingFragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null)
            title = getArguments().getString(title);
    }


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        PreferenceManager manager = new PreferenceManager(getContext());

        stcNotification.setChecked(manager.getNotification());

        if (manager.getLanguage()){
            btnCangelanguage.setText(getString(R.string.english));
        }else {
            btnCangelanguage.setText(getString(R.string.bengali));
        }

        stcNotification.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {

                manager.setNotification(b);
            }
        });
    }

    @OnClick(R.id.btnChangeLanguage)
    public void changeLanguage(){
        //do logic here to change language
    }
}
