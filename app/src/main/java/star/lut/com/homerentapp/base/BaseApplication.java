package star.lut.com.homerentapp.base;

import android.app.Application;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import com.crashlytics.android.Crashlytics;
import io.fabric.sdk.android.Fabric;
import star.lut.com.homerentapp.BuildConfig;
import star.lut.com.homerentapp.constants.ValueConstants;
import star.lut.com.homerentapp.db.greenDao.DaoMaster;
import star.lut.com.homerentapp.db.greenDao.DaoSession;
import timber.log.Timber;

/**
 * Created by kamrulhasan on 17/8/18.
 */
public class BaseApplication extends Application {

    private static BaseApplication instance;

    private DaoSession mDaoSession;

    @Override
    public void onCreate() {
        super.onCreate();
        Fabric.with(this, new Crashlytics());

        instance = this;

        //green dao
        mDaoSession = new DaoMaster(
                new DaoMaster.DevOpenHelper(this, ValueConstants.DATABASE_DIA).getWritableDatabase()).newSession();

        //configure timber for logging
        if (BuildConfig.DEBUG) {
            Timber.plant(new Timber.DebugTree());
        }
    }

    public DaoSession getDaoSession(){
        return mDaoSession;
    }

    public static BaseApplication getInstance() {
        return instance;
    }

    public static boolean hasNetwork() {
        return getInstance().checkIfHasNetwork();
    }

    public boolean checkIfHasNetwork() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = cm.getActiveNetworkInfo();
        return networkInfo != null && networkInfo.isConnected();
    }
}