package star.lut.com.homerentapp.constants;

/**
 * Created by kamrulhasan on 18/8/18.
 */
public interface ValueConstants {
    String KEY_RENT_LIST_ID = "rent_id";

    String DATABASE_DIA = "valobasha_dia";

    int KEY_MARKER_FOR_SELECTED_LOCATION = 33;

    int RESULT_CODE_FROM_SEARCH = 5555;
    int RESULT_CODE_TO_SEARCH = 4444;
    int RESULT_CODE_SELECT_RENT_LOCATION = 1111;
    int RESULT_CODE_FOR_STORAGE_PERMISSION = 2222;

    int RESULT_CODE_SORT_LOCATION = 100;

    String RENT_TYPE_FAMILY_NOT_SHARED = "family_home";
    String RENT_TYPE_FAMIL_SHARED = "shared_room";
    String RENT_TYPE_OFFICE_SPACE = "office_space";
    String RENT_TYPE_GARAGE = "garage";
}
