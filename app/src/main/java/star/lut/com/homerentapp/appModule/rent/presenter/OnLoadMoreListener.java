package star.lut.com.homerentapp.appModule.rent.presenter;

/**
 * Created by kamrulhasan on 17/8/18.
 */
public interface OnLoadMoreListener {
    void onLoadMore();
}