package star.lut.com.homerentapp.appModule.rent.view;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.transition.TransitionManager;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import star.lut.com.homerentapp.R;
import star.lut.com.homerentapp.appModule.rent.adapter.PictureRvAdapter;
import star.lut.com.homerentapp.appModule.rent.adapter.UploadedPictureRvAdapter;
import star.lut.com.homerentapp.appModule.rent.presenter.RentUploadPresenter;
import star.lut.com.homerentapp.appModule.rent.presenter.RentUploadViewInterface;
import star.lut.com.homerentapp.base.BaseActivity;
import star.lut.com.homerentapp.constants.ApiConstants;
import star.lut.com.homerentapp.constants.ValueConstants;
import star.lut.com.homerentapp.dataModel.Picture;
import star.lut.com.homerentapp.dataModel.rent.RentDetailsModel;
import star.lut.com.homerentapp.dataModel.sendModel.RentLocationSendModel;
import star.lut.com.homerentapp.dataModel.sendModel.RentSendModel;

public class RentUploadActivity extends BaseActivity implements RentUploadViewInterface{
    private RentUploadPresenter presenter;

    @BindView(R.id.rootView)
    public ConstraintLayout rootView;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.tvFragmentTitle)
    TextView tvFragmentTitle;
    @BindView(R.id.tilTitle) public TextInputLayout tilTitle;
    @BindView(R.id.tilDescription) public TextInputLayout tilDescription;
    @BindView(R.id.tilAmount) public TextInputLayout tilAmount;
    @BindView(R.id.tvRentType) public TextView tvRentType;
    @BindView(R.id.rgRentType) public RadioGroup rgRentType;
    @BindView(R.id.tilRoomNo) public TextInputLayout tilRoomNo;
    @BindView(R.id.tilApartSize) public TextInputLayout tilApartSize;
    @BindView(R.id.tilNearByPlaces) public TextInputLayout tilNearbyPlaces;
    @BindView(R.id.tvUpload) public TextView tvUploadPic;
    @BindView(R.id.btnUpload) public Button btnUpload;
    @BindView(R.id.rvPictures) public RecyclerView rvPictures;
    @BindView(R.id.tvLcoation) public TextView tvLocation;
    @BindView(R.id.btnLocation) public Button btnLocation;

    private RentSendModel model;
    private List<Picture> pictures;
    private List<File> files;
    private List<Uri> uris;
    private RentLocationSendModel location;
    private String rent_type = ValueConstants.RENT_TYPE_FAMILY_NOT_SHARED;

    private String rent_id;
    private int current_uploading_picture_position;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setSupportActionBar(toolbar);
        tvFragmentTitle.setText(getString(R.string.menu_post_add));

        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null){
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setHomeAsUpIndicator(R.drawable.ic_arrow_back_black_24px);
        }

        presenter = new RentUploadPresenter(this, this);
        hideSoftKey();

        initView();
    }

    @Override
    public int getLayout() {
        return R.layout.activity_rent_upload;
    }

    @Override
    public void onRentLocationUploaded(String locationId) {
        model.locationid = locationId;

        presenter.uploadRentDetails(model);
    }

    @Override
    public void onRentDetailsUploaded(String rentID) {
        this.rent_id = rentID;

//        uploadPicture();
    }

    private void uploadPicture() {
        File file = new File(uris.get(uris.size() - 1).getPath());

        presenter.uploadPicture(file, rent_id);
    }

    @Override
    public void onPictureSelected(List<Uri> pictures) {
        if (pictures != null) {
            this.pictures = new ArrayList<>();

            uris = pictures;

            for (int i = 0; i < pictures.size(); i++) {
                Picture picture = new Picture();
                picture.pictureUrl = pictures.get(i).getPath();
                this.pictures.add(picture);
            }

            if (this.pictures != null) {
                initRvAdapter();
            }

            rvPictures.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onPictureUploaded() {
        if (uris.size() > 0) {
            uris.remove(uris.size() - 1);
            uploadPicture();
        }else {
            Toast.makeText(this, "Ad posted" , Toast.LENGTH_SHORT).show();
            finish();
        }
    }

    private void initRvAdapter() {
        UploadedPictureRvAdapter adapter = new UploadedPictureRvAdapter(this, pictures, new UploadedPictureRvAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(Picture picture) {

            }
        });
        rvPictures.setAdapter(adapter);
        adapter.notifyDataSetChanged();
        if (adapter.getItemCount() == 0){
            rvPictures.setVisibility(View.GONE);
        }
    }

    private void hideSoftKey() {
        InputMethodManager imm = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);
        //Find the currently focused view, so we can grab the correct window token from it.
        View view = getCurrentFocus();
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = new View(this);
        }
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return super.onSupportNavigateUp();
    }

    private void initView(){
        model = new RentSendModel();
        initRv();
        rvPictures.setVisibility(View.GONE);
        processRentType();
    }

    private void initRv(){
        rvPictures.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        rvPictures.setHasFixedSize(true);
        rvPictures.setItemAnimator(new DefaultItemAnimator());
        rvPictures.setNestedScrollingEnabled(false);
        rvPictures.addItemDecoration(new DividerItemDecoration(this, LinearLayoutManager.HORIZONTAL));
    }

    private void processRentType(){
        rgRentType.setOnCheckedChangeListener((group, checkedId) -> {
            if (checkedId == R.id.rbFlat) {
                TransitionManager.beginDelayedTransition(rootView);
                tilApartSize.setVisibility(View.VISIBLE);
                tilRoomNo.setVisibility(View.VISIBLE);
                rent_type = ValueConstants.RENT_TYPE_FAMILY_NOT_SHARED;
            } else if (checkedId == R.id.rbSharedRoom){
                TransitionManager.beginDelayedTransition(rootView);
                tilApartSize.setVisibility(View.VISIBLE);
                tilRoomNo.setVisibility(View.VISIBLE);
                rent_type = ValueConstants.RENT_TYPE_FAMIL_SHARED;
            } else if (checkedId == R.id.rbOfficeSpace){
                TransitionManager.beginDelayedTransition(rootView);
                tilApartSize.setVisibility(View.VISIBLE);
                tilRoomNo.setVisibility(View.GONE);
                rent_type = ValueConstants.RENT_TYPE_OFFICE_SPACE;
            }else if (checkedId == R.id.rbGarage){
                TransitionManager.beginDelayedTransition(rootView);
                tilApartSize.setVisibility(View.GONE);
                tilRoomNo.setVisibility(View.GONE);
                rent_type = ValueConstants.RENT_TYPE_GARAGE;
            }
        });
    }

    @OnClick(R.id.btnUpload)
    public void selectPicture(){
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.READ_EXTERNAL_STORAGE)) {
                showExplanation();
            } else {
                requestPermission();
            }
        }else {
            presenter.selectPictures();
        }
    }
    private void requestPermission() {
        ActivityCompat.requestPermissions(this,
                new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, ValueConstants.RESULT_CODE_FOR_STORAGE_PERMISSION);
    }


    private void showExplanation() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(getString(R.string.storage_permission_title))
                .setMessage(getString(R.string.storage_permission_explaination))
                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        requestPermission();
                    }
                });
        builder.create().show();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case ValueConstants.RESULT_CODE_FOR_STORAGE_PERMISSION:
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    presenter.selectPictures();
                } else {
                    Toast.makeText(this, getString(R.string.permission_denied), Toast.LENGTH_SHORT).show();
                }
        }
    }

    @OnClick(R.id.btnLocation)
    public void selectLocation(){
        PlacePicker.IntentBuilder builder = new PlacePicker.IntentBuilder();
        try {
            startActivityForResult(builder.build(this), ValueConstants.RESULT_CODE_SELECT_RENT_LOCATION);
        } catch (GooglePlayServicesRepairableException e) {
            e.printStackTrace();
        } catch (GooglePlayServicesNotAvailableException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK){
            if (requestCode == ValueConstants.RESULT_CODE_SELECT_RENT_LOCATION){
                Place place = PlacePicker.getPlace(data, this);
                if (place.getLatLng() != null){
                    location = new RentLocationSendModel();
                    location.api = ApiConstants.API_INSERT_LOCATION;
                    location.lat = place.getLatLng().latitude;
                    location.lon = place.getLatLng().longitude;
                    location.locationName = place.getAddress().toString();
                    tvLocation.setText(getString(R.string.rent_location)+" "+location.locationName);
                }
            }
        }
    }

    @OnClick(R.id.btnDone)
    public void done(){
        processData();
    }

    private void processData() {
        boolean goAhead = false;
        if (invalidateData(tilTitle) | invalidateData(tilAmount) | invalidateData(tilNearbyPlaces) | invalidateData(tilDescription)){
            if (rent_type.equals(ValueConstants.RENT_TYPE_FAMILY_NOT_SHARED)){
                if (invalidateData(tilApartSize) | invalidateData(tilRoomNo))
                    goAhead = true;
            }else if (rent_type.equals(ValueConstants.RENT_TYPE_FAMIL_SHARED)){
                if (invalidateData(tilApartSize) | invalidateData(tilRoomNo))
                    goAhead = true;
            }else if (rent_type.equals(ValueConstants.RENT_TYPE_OFFICE_SPACE)){
                if (invalidateData(tilApartSize))
                    goAhead = true;
            }else{
                goAhead = true;
            }
        }

        if (goAhead)
            getDatas();
    }

    private void getDatas() {
        String title = tilTitle.getEditText().getText().toString().trim();
        String description = tilDescription.getEditText().getText().toString().trim();
        String amount = tilAmount.getEditText().getText().toString().trim();
        String nearByPlace = tilNearbyPlaces.getEditText().getText().toString().trim();
        String apartmentSize = "", roomNo = "";
        if (rent_type.equals(ValueConstants.RENT_TYPE_FAMILY_NOT_SHARED)){
            apartmentSize = tilApartSize.getEditText().getText().toString().trim();
            roomNo = tilApartSize.getEditText().getText().toString().trim();
        }else if (rent_type.equals(ValueConstants.RENT_TYPE_FAMIL_SHARED)){
            apartmentSize = tilApartSize.getEditText().getText().toString().trim();
            roomNo = tilApartSize.getEditText().getText().toString().trim();
        }else if (rent_type.equals(ValueConstants.RENT_TYPE_OFFICE_SPACE)){
            apartmentSize = tilApartSize.getEditText().getText().toString().trim();
        }

        model.title = title;
        model.details = description;
        model.amount = amount;
        model.nearby = nearByPlace;
        model.apartid = rent_type;
        model.aprt_size = apartmentSize;
        model.room_no = roomNo;
        model.api = ApiConstants.API_INSERT_RENT;

        presenter.uploadLocation(location);
    }

    private boolean invalidateData(TextInputLayout textInputLayout) {
        if (textInputLayout.getEditText().getText().toString().trim().length() == 0){
            textInputLayout.setErrorEnabled(true);
            textInputLayout.setError(getString(R.string.null_field));
            return false;
        } else {
            return true;
        }
    }
}
