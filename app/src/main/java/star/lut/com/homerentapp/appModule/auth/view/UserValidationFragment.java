package star.lut.com.homerentapp.appModule.auth.view;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.FragmentManager;
import android.view.View;

import butterknife.BindView;
import butterknife.OnClick;
import star.lut.com.homerentapp.R;
import star.lut.com.homerentapp.appModule.auth.presenter.NewPasswordPresenter;
import star.lut.com.homerentapp.appModule.auth.presenter.UserValidationPresenter;
import star.lut.com.homerentapp.appModule.auth.presenter.UserValidationViewInterface;
import star.lut.com.homerentapp.base.BaseFragment;

public class UserValidationFragment extends BaseFragment implements UserValidationViewInterface{
    private static final String ARG_TITLE = "title";
    private String title;

    private UserValidationPresenter presenter;

    @BindView(R.id.tilVerifyCode) public TextInputLayout tilVerifyCode;
    private String code;
    private String phone;

    @Override
    public int getLayoutId() {
        return R.layout.fragment_sms_verification;
    }

    public UserValidationFragment() {
    }

    public void setData(String code, String phone){
        this.code = code;
        this.phone = phone;
    }

    public static UserValidationFragment newInstance(String title){
        UserValidationFragment singUpFragment = new UserValidationFragment();
        Bundle args = new Bundle();
        args.putString(ARG_TITLE, title);
        singUpFragment.setArguments(args);
        return singUpFragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null)
            title = getArguments().getString(ARG_TITLE);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        presenter = new UserValidationPresenter(getContext(), this);

        if (code != null){
            tilVerifyCode.getEditText().setText(code);
        }
    }

    @OnClick(R.id.btnSubmit)
    public void verifyCode(){
        if (validateSmsCode()){
            presenter.verifyCode(code, phone);
        }
    }

    private boolean validateSmsCode() {
        code = tilVerifyCode.getEditText().getText().toString().trim();
        if (code.isEmpty()){
            tilVerifyCode.setErrorEnabled(true);
            tilVerifyCode.setError("Sms Code"+getString(R.string.null_field));
            return false;
        }else if (code.length() < 3){
            tilVerifyCode.setErrorEnabled(true);
            tilVerifyCode.setError(getString(R.string.sms_code_not_same_size));
            return false;
        } else {
            tilVerifyCode.setErrorEnabled(false);
            tilVerifyCode.setError(null);
            return true;
        }
    }

    @Override
    public void onCodeVerified() {
//        if (resetPassword){
//            NewPasswordFragment userValidationFragment = NewPasswordFragment.newInstance("Login");
//            FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
//            fragmentManager.beginTransaction()
//                    .replace(R.id.fragmentContainer, userValidationFragment)
//                    .commit();
//        }else {
            LoginFragment userValidationFragment = LoginFragment.newInstance("Login");
            FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
            fragmentManager.beginTransaction()
                    .replace(R.id.fragmentContainer, userValidationFragment)
                    .commit();
//        }
    }

    @Override
    public void onCodeInvalid() {
        tilVerifyCode.setErrorEnabled(true);
        tilVerifyCode.setError("Sms Code"+getString(R.string.sms_code_invalid));
    }

    
}
