package star.lut.com.homerentapp.dataModel.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

import star.lut.com.homerentapp.dataModel.rent.RentDetailsModel;

/**
 * Created by kamrulhasan on 21/10/18.
 */
public class RentResponse {
    @SerializedName("rent_details")
    @Expose
    public RentDetailsModel rentDetails;

    @Override
    public String toString() {
        return "RentResponse{" +
                "rentDetails=" + rentDetails +
                '}';
    }
}
