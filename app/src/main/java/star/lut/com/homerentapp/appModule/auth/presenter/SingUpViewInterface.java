package star.lut.com.homerentapp.appModule.auth.presenter;

/**
 * Created by kamrulhasan on 17/8/18.
 */
public interface SingUpViewInterface {
    void onRegistrationSuccessful(String code);
    void onRegistrationUnsuccessful(String code);
    void onRegistrationUnsuccessful();
}
