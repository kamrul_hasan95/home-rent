package star.lut.com.homerentapp.appModule.rent.view;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import star.lut.com.homerentapp.R;
import star.lut.com.homerentapp.appModule.rent.adapter.RentRvAdapter;
import star.lut.com.homerentapp.appModule.rent.presenter.RentPresenter;
import star.lut.com.homerentapp.appModule.rent.presenter.RentViewInterface;
import star.lut.com.homerentapp.base.BaseFragment;
import star.lut.com.homerentapp.constants.ValueConstants;
import star.lut.com.homerentapp.customHelper.DisplayMatrics;
import star.lut.com.homerentapp.dataModel.rent.RentDetailsModel;
import star.lut.com.homerentapp.dataModel.response.RentResponse;
import timber.log.Timber;

/**
 * Created by kamrulhasan on 17/8/18.
 */
public class RentFragment extends BaseFragment implements RentViewInterface, OnMapReadyCallback {
    private static final String ARG_TITLE = "title";
    private String title;
    private RentPresenter presenter;
    private RentRvAdapter adapter;
    private List<RentDetailsModel> rentModels;
    private GoogleMap mMap;
    private boolean isFav;

    @BindView(R.id.rvRent) public RecyclerView rvRent;


    @Override
    public int getLayoutId() {
        return R.layout.fragement_rent;
    }

    public RentFragment() {
    }

    public RentFragment newInstance(String title){
        RentFragment rentFragment = new RentFragment();
        Bundle args = new Bundle();
        args.putString(ARG_TITLE, title);
        rentFragment.setArguments(args);
        return rentFragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null)
            title = getArguments().getString(title);
    }

    public void setFavScreen(boolean isFav){
        this.isFav = isFav;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        presenter = new RentPresenter(getContext(), this);

        SupportMapFragment mapFragment = (SupportMapFragment) this.getChildFragmentManager()
                .findFragmentById(R.id.mapFragmentRentList);

        mapFragment.getMapAsync(this);

        rentModels = new ArrayList<>();

        if (isFav){
            presenter.getFavlist();
        }else {
            presenter.getRentList(0);
        }
        initRv();
    }

    private void initRv(){
        rvRent.setHasFixedSize(true);
        rvRent.setLayoutManager(new LinearLayoutManager(getContext()));

        initRVData();
    }

    private void initRVData(){
        adapter = new RentRvAdapter(getContext(), rentModels, new RentRvAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(RentDetailsModel rentModel, int position) {
                Intent intent = new Intent(getContext(), RentDetailsActivity.class);
                intent.putExtra(ValueConstants.KEY_RENT_LIST_ID, rentModel.rentId);
                startActivity(intent);
            }
        }, new RentRvAdapter.OnLoadingMore() {
            @Override
            public void onLoadingMode(int size, int no) {
                presenter.getRentList(no);
            }
        });

        rvRent.setAdapter(adapter);
    }

    @Override
    public void onNewRentList(List<RentDetailsModel> rentResponse) {
        Timber.d("asdf"+rentResponse.toString());
        rentModels.addAll(rentResponse);

        Timber.d("asdf"+rentModels.toString());
        adapter.notifyDataSetChanged();
//        initRVData();
        initMapData();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }else {
            mMap.setMyLocationEnabled(true);
            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(23.8103, 90.4125), 14));
        }


    }

    private void initMapData(){
        LatLng rentLatLng = new LatLng(23.8103,90.4125);

        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(rentLatLng, 10));

        for (int i = 0 ; i < rentModels.size() ; i++){
            if (rentModels.get(i).location.lat.length() != 0 && rentModels.get(i).location.lon.length() != 0) {
                try {
                    rentLatLng = new LatLng(Double.parseDouble(rentModels.get(i).location.lat), Double.parseDouble(rentModels.get(i).location.lon));

                    MarkerOptions markerOptions = new MarkerOptions()
                            .position(rentLatLng);

                    mMap.addMarker(markerOptions);
                }catch (NumberFormatException e){
                    e.printStackTrace();
                }
            }
        }
    }
}
