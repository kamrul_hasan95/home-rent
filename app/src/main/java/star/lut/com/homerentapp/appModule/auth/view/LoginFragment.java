package star.lut.com.homerentapp.appModule.auth.view;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.OnClick;
import star.lut.com.homerentapp.R;
import star.lut.com.homerentapp.appModule.auth.presenter.LoginPresenter;
import star.lut.com.homerentapp.appModule.auth.presenter.LoginViewInterface;
import star.lut.com.homerentapp.appModule.mother.view.MotherActivity;
import star.lut.com.homerentapp.base.BaseFragment;

/**
 * Created by kamrulhasan on 17/8/18.
 */
public class LoginFragment extends BaseFragment implements LoginViewInterface {
    private static final String ARG_TITLE = "title";
    private String title;
    private LoginPresenter loginPresenter;

    private View view;

    @BindView(R.id.tvCreateNewAccount) public TextView tvCreateNewAccount;
    @BindView(R.id.tvForgotPassword) public TextView tvForgotPassword;
    @BindView(R.id.tilUserName) public TextInputLayout tilUserName;
    @BindView(R.id.tilPassword) public TextInputLayout tilPassword;
    
    private String username, password;

    @Override
    public int getLayoutId() {
        return R.layout.fragment_login;
    }

    public LoginFragment() {
    }

    public static LoginFragment newInstance(String title){
        LoginFragment loginFragment = new LoginFragment();
        Bundle args = new Bundle();
        args.putString(ARG_TITLE, title);
        loginFragment.setArguments(args);
        return loginFragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null)
            title = getArguments().getString(title);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        this.view = view;

        loginPresenter = new LoginPresenter(getContext(), this);

        initView();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    private void initView(){
        makeSingupTestClickable();
        makeForgotTextClickable();
    }

    private void makeSingupTestClickable(){
        SpannableString ss = new SpannableString(getString(R.string.no_account)+"\n"+getString(R.string.create_account));
        ClickableSpan clickableSpan = new ClickableSpan() {
            @Override
            public void onClick(View textView) {
                loadSingUpFragment();
            }

            @Override
            public void updateDrawState(TextPaint ds) {
                super.updateDrawState(ds);
                ds.setUnderlineText(false);
            }
        };
        ss.setSpan(clickableSpan, 0, ss.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

        tvCreateNewAccount.setText(ss);
        tvCreateNewAccount.setMovementMethod(LinkMovementMethod.getInstance());
        tvCreateNewAccount.setHighlightColor(getResources().getColor(R.color.colorPrimary));
    }

    @OnClick(R.id.ibBack)
    public void loadSingUpFragment(){
        SingUpFragment singUpFragment = new SingUpFragment();
        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
        fragmentManager.beginTransaction()
                .replace(R.id.fragmentContainer, singUpFragment)
                .commit();
    }

    private void makeForgotTextClickable(){
        SpannableString ss = new SpannableString(getString(R.string.forgot_password));
        ClickableSpan clickableSpan = new ClickableSpan() {
            @Override
            public void onClick(View textView) {
                ForgotPasswordFragment forgotPasswordFragment = new ForgotPasswordFragment();
                FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                fragmentManager.beginTransaction()
                        .replace(R.id.fragmentContainer, forgotPasswordFragment)
                        .commit();
            }

            @Override
            public void updateDrawState(TextPaint ds) {
                super.updateDrawState(ds);
                ds.setUnderlineText(false);
            }
        };
        ss.setSpan(clickableSpan, 0, ss.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

        tvForgotPassword.setText(ss);
        tvForgotPassword.setMovementMethod(LinkMovementMethod.getInstance());
        tvForgotPassword.setHighlightColor(getResources().getColor(R.color.colorPrimary));
    }

    @OnClick(R.id.btnLogin)
    public void login(){
        if (validatePassword() | validateUserName()){
            loginPresenter.login(username, password);
        }else{
            password = null;
            username = null;
        }
    }

    private boolean validateUserName(){
        username = tilUserName.getEditText().getText().toString().trim();
        if (username.isEmpty()){
            tilUserName.setErrorEnabled(true);
            tilUserName.setError("UserName"+getString(R.string.null_field));
            return false;
        }else if(username.length() > 15){
            tilUserName.setErrorEnabled(true);
            tilUserName.setError(getString(R.string.username_long));
            return false;
        }
        else {
            tilUserName.setErrorEnabled(false);
            tilUserName.setError(null);
            return true;
        }
    }

    private boolean validatePassword(){
        password = tilPassword.getEditText().getText().toString().trim();
        if (password.isEmpty()){
            tilPassword.setErrorEnabled(true);
            tilPassword.setError("Password"+getString(R.string.null_field));
            return false;
        } else if (password.length() < 8){
            tilPassword.setErrorEnabled(true);
            tilPassword.setError("Password"+getString(R.string.password_short));
            return false;
        } else {
            tilPassword.setErrorEnabled(false);
            tilPassword.setError(null);
            return true;
        }
    }

    @Override
    public void onLoginSucess() {
        ContextCompat.startActivity(getActivity(), new Intent(getContext(), MotherActivity.class), ActivityOptionsCompat.makeClipRevealAnimation(view, 10, 10, 20 , 20).toBundle());
        getActivity().finish();
    }

    @Override
    public void onLoginNotVerified(String code) {
        UserValidationFragment userValidationFragment = UserValidationFragment.newInstance("User Validation");
        userValidationFragment.setData(code, "12345 b");
        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
        fragmentManager.beginTransaction()
                .replace(R.id.fragmentContainer, userValidationFragment)
                .commit();
    }
}
