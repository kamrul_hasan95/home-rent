package star.lut.com.homerentapp.apiService;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import io.reactivex.Observable;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Response;
import retrofit2.http.Body;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

/**
 * Created by kamrulhasan on 5/11/18.
 */
public interface UploadWebService {
    @Multipart
    @POST("upload_picture.php")
    Observable<Response<JsonElement>> rentPicture(
            @Body RequestBody body

            );

    @Multipart
    @POST("upload_picture.php")
    Observable<Response<JsonObject>> uploadProfilePicture(
            @Part("file") RequestBody picPart,
            @Part("name") RequestBody name,
            @Part("where") RequestBody where,
            @Part("mobile") RequestBody userId,
            @Part("aprt_id") RequestBody apart_id
    );
}
