package star.lut.com.homerentapp.customHelper;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Point;
import android.os.Build;
import android.support.annotation.NonNull;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.WindowManager;

import star.lut.com.homerentapp.R;
import star.lut.com.homerentapp.base.BaseApplication;

/**
 * Created by kamrulhasan on 17/9/18.
 */
public class DisplayMatrics {
    int mRealSizeHeight = 0;
    int mRealSizeWidth = 0;


    @NonNull
    public int getDisplayDimensions( Context context )
    {
        WindowManager windowManager =
                (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        final Display display = windowManager.getDefaultDisplay();
        Point outPoint = new Point();
        if (Build.VERSION.SDK_INT >= 19) {
            // include navigation bar
            display.getRealSize(outPoint);
        } else {
            // exclude navigation bar
            display.getSize(outPoint);
        }
        if (outPoint.y > outPoint.x) {
            mRealSizeHeight = outPoint.y;
            mRealSizeWidth = outPoint.x;
        } else {
            mRealSizeHeight = outPoint.x;
            mRealSizeWidth = outPoint.y;
        }

        final TypedArray styledAttributes = context.getTheme().obtainStyledAttributes(
                new int[] { android.R.attr.actionBarSize });
        int mActionBarSize = (int) styledAttributes.getDimension(0, 0);

        mRealSizeHeight = mRealSizeHeight - mActionBarSize - 2;

        return mRealSizeHeight;
    }

}
