package star.lut.com.homerentapp.appModule.auth.presenter;

/**
 * Created by kamrulhasan on 17/8/18.
 */
public interface LoginViewInterface {
    void onLoginSucess();
    void onLoginNotVerified(String code);
}
