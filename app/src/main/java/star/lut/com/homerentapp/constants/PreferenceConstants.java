package star.lut.com.homerentapp.constants;

/**
 * Created by kamrulhasan on 21/10/18.
 */
public interface PreferenceConstants {
    String PREFERENCE_NAME = "valobasha_app_pref";

    String USER_LOGGED_IN = "user_logged_in";

    String NOTIFICATION_SETTINGS = "notification_settings";
    String LANGUAGE_ENGLISH = "set_language_english";

    String ACCESS_TOKEN = "access_token";
    String USER_NAME = "user_name";
    String USER_PHONE = "user_phone";
    String USER_EMAIL = "user_email";
    String USER_PIC = "user_pic";
}
