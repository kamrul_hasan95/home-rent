package star.lut.com.homerentapp.apiService;

import com.google.gson.JsonArray;

import java.util.List;

import retrofit2.Response;
import retrofit2.http.Body;
import retrofit2.http.GET;
import io.reactivex.Observable;
import retrofit2.http.POST;
import star.lut.com.homerentapp.dataModel.response.LocationResponse;
import star.lut.com.homerentapp.dataModel.response.RentResponse;
import star.lut.com.homerentapp.dataModel.response.RentUploadResponse;
import star.lut.com.homerentapp.dataModel.sendModel.FavRentList;
import star.lut.com.homerentapp.dataModel.sendModel.RentList;
import star.lut.com.homerentapp.dataModel.sendModel.RentLocationSendModel;
import star.lut.com.homerentapp.dataModel.sendModel.RentSendModel;

/**
 * Created by kamrulhasan on 18/8/18.
 */
public interface RentWebService {

    //get all rents
    @POST("rent.php")
    Observable<List<RentResponse>> getRentList(
            @Body RentList rentList
            );

    //to get particular rent by id
    @POST("rent.php")
    Observable<List<RentResponse>> getfavRentList(
            @Body FavRentList favRentList
            );


    //post location
    @POST("rent.php")
    Observable<Response<List<LocationResponse>>> setRentLocation(
            @Body RentLocationSendModel favRentList
    );


    //post rent
    @POST("rent.php")
    Observable<Response<List<RentUploadResponse>>> setRentDetails(
            @Body RentSendModel favRentList
    );
}
