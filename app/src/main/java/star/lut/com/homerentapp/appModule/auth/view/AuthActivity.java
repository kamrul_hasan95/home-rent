package star.lut.com.homerentapp.appModule.auth.view;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;

import butterknife.OnClick;
import star.lut.com.homerentapp.R;
import star.lut.com.homerentapp.appModule.auth.presenter.AuthPresenter;
import star.lut.com.homerentapp.appModule.auth.presenter.AuthViewInterface;
import star.lut.com.homerentapp.appModule.mother.view.MotherActivity;
import star.lut.com.homerentapp.base.BaseActivity;

public class AuthActivity extends BaseActivity implements AuthViewInterface {
    private AuthPresenter authPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        authPresenter = new AuthPresenter(this, this);
        showSingUpFragment();
    }

    @Override
    public int getLayout() {
        return R.layout.activity_auth;
    }

    private void showSingUpFragment(){
        SingUpFragment fragmentSignUp = new SingUpFragment();
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction()
                .replace(R.id.fragmentContainer, fragmentSignUp)
                .commit();
    }

    @Override
    public void onBackPressed() {


        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(R.string.exit_app)
                .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        AuthActivity.super.onBackPressed();
                    }
                })
                .setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // User cancelled the dialog
                    }
                });
        builder.create();
    }

    @OnClick(R.id.ibHome)
    public void goToHome(){
        Intent intent = new Intent(this, MotherActivity.class);
        startActivity(intent);
    }
}
