package star.lut.com.homerentapp.appModule.auth.presenter;

/**
 * Created by kamrulhasan on 10/9/18.
 */
public interface UserValidationViewInterface {
    void onCodeVerified();
    void onCodeInvalid();
}
