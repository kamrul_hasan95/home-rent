package star.lut.com.homerentapp.appModule.auth.presenter;

import android.content.Context;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.JsonObject;

import java.util.List;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Response;
import star.lut.com.homerentapp.R;
import star.lut.com.homerentapp.apiService.AuthWebService;
import star.lut.com.homerentapp.apiService.WebServiceFactory;
import star.lut.com.homerentapp.base.BaseApplication;
import star.lut.com.homerentapp.dataModel.response.LoginResponseModel;
import star.lut.com.homerentapp.dataModel.sendModel.LoginModel;
import star.lut.com.homerentapp.db.PreferenceManager;
import star.lut.com.homerentapp.db.greenDao.Profile;
import timber.log.Timber;

/**
 * Created by kamrulhasan on 17/8/18.
 */
public class LoginPresenter {
    private Context context;
    private LoginViewInterface loginViewInterface;
    private AuthWebService authWebService;

    public LoginPresenter(Context context, LoginViewInterface loginViewInterface) {
        this.context = context;
        this.loginViewInterface = loginViewInterface;
    }

    public void login(String userName, String password) {
        authWebService = WebServiceFactory.createRetrofitService(AuthWebService.class);

        LoginModel loginModel = new LoginModel(userName, password);

        authWebService.loginUser(loginModel)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<Response<List<JsonObject>>>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(Response<List<JsonObject>> jsonObjectResponse) {
                        Timber.d("loginPresenter0" + jsonObjectResponse);

                        if (jsonObjectResponse.isSuccessful()) {
                            JsonObject jsonObject = jsonObjectResponse.body().get(0);
                            boolean status = jsonObject.get("status").getAsBoolean();
                            Timber.d("loginPresenter1" + status);

                            try{
                                if (status) {
                                    LoginResponseModel model = new Gson().fromJson(jsonObject, LoginResponseModel.class);
                                    Timber.d("loginpresenter2" + model);
                                    saveProfileToDB(model);
                                    //do the logic to login
                                } else if (jsonObject.get("error_code").toString().equals("NOT_VERIFIED")) {
                                    String sms = jsonObject.get("sms_code").toString();
                                    loginViewInterface.onLoginNotVerified(sms);
                                }
                            }catch (NullPointerException e){
                                e.printStackTrace();
                            }
                        } else {
                            Toast.makeText(context, context.getString(R.string.someting_went_wrong), Toast.LENGTH_LONG).show();
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                    }

                    @Override
                    public void onComplete() {

                    }
                });

    }

    private void saveProfileToDB(LoginResponseModel model) {
        ((BaseApplication) context.getApplicationContext()).getDaoSession().clear();
        Profile profile = new Profile();
        profile.name = model.profile.name;
        profile.backId = model.profile.backId;
        profile.email = model.profile.email;
        profile.pic = model.profile.pic;
        profile.token = model.profile.token;
        profile.mobileNumber = model.profile.mobileNumber;

        ((BaseApplication) context.getApplicationContext()).getDaoSession().getProfileDao().insert(profile);
        PreferenceManager preferenceManager = new PreferenceManager(context);
        preferenceManager.setUserLoggedIn(true);

        loginViewInterface.onLoginSucess();
    }
}
