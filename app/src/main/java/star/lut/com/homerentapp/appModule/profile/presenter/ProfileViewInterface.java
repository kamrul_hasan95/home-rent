package star.lut.com.homerentapp.appModule.profile.presenter;

import android.net.Uri;

public interface ProfileViewInterface {
    void onImageSelected(Uri uri);
    void onPictureUploaded(String fileName);
}
