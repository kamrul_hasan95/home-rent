package star.lut.com.homerentapp.utils;

import android.content.Context;

import com.google.gson.JsonObject;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Response;
import star.lut.com.homerentapp.apiService.AuthWebService;
import star.lut.com.homerentapp.apiService.WebServiceFactory;
import star.lut.com.homerentapp.db.PreferenceManager;

/**
 * Created by kamrulhasan on 22/10/18.
 */
public class TokenVerifier {
    private Context context;

    public TokenVerifier(Context context) {
        this.context = context;
    }

    public void refreshToken(){
        PreferenceManager manager = new PreferenceManager(context);
        String token = manager.getToken();

        AuthWebService webService = WebServiceFactory.createRetrofitService(AuthWebService.class);

        webService.verifyToken(token)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<Response<JsonObject>>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(Response<JsonObject> jsonObjectResponse) {

                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }
}
