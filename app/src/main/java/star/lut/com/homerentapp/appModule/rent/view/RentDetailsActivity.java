package star.lut.com.homerentapp.appModule.rent.view;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import star.lut.com.homerentapp.R;
import star.lut.com.homerentapp.appModule.rent.adapter.PictureRvAdapter;
import star.lut.com.homerentapp.appModule.rent.presenter.RentDetailsPresenter;
import star.lut.com.homerentapp.appModule.rent.presenter.RentDetailsViewInterface;
import star.lut.com.homerentapp.base.BaseActivity;
import star.lut.com.homerentapp.constants.ValueConstants;
import star.lut.com.homerentapp.dataModel.Picture;
import star.lut.com.homerentapp.dataModel.rent.RentDetailsModel;

public class RentDetailsActivity extends BaseActivity implements RentDetailsViewInterface, OnMapReadyCallback, GoogleMap.OnMarkerClickListener {
    private RentDetailsPresenter presenter;
    private GoogleMap mMap;

    @BindView(R.id.toolbar) public Toolbar toolbar;
    @BindView(R.id.tvFragmentTitle) public TextView tvFragmentTitle;
    @BindView(R.id.ivRentPicture) public ImageView ivRentPicture;
    @BindView(R.id.rvPictures) public RecyclerView rvPictures;
    @BindView(R.id.tvTitle) public TextView tvTitle;
    @BindView(R.id.tvDetails) public TextView tvDetails;
    @BindView(R.id.tvLocation) public TextView tvLocation;
    @BindView(R.id.tvType) public TextView tvType;
    @BindView(R.id.tvRentAmount) public TextView tvRentAmount;
    @BindView(R.id.tvRoomNumbers) public TextView tvRoomNumber;
    @BindView(R.id.tvApartSize) public TextView tvAprtSize;
    @BindView(R.id.ivFavorite) public ImageView ivFavorite;
    @BindView(R.id.btnBook) public Button btnBook;


    private String selectedRentId;
    private List<Picture> pictures;
    private RentDetailsModel rentDetails;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setSupportActionBar(toolbar);
        toolbar.setTitle("");
        if (getSupportActionBar() != null)
            getSupportActionBar().setDisplayShowTitleEnabled(false);

        tvFragmentTitle.setText(getString(R.string.rent_details_title));

        presenter = new RentDetailsPresenter(this, this);
        Bundle bundle = getIntent().getExtras();
        selectedRentId = bundle.getString(ValueConstants.KEY_RENT_LIST_ID);

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.mapFragmentRentDetails);

        mapFragment.getMapAsync(this);

        presenter.getRentDetails(selectedRentId);
    }

    @Override
    public int getLayout() {
        return R.layout.activity_rent_details;
    }

    private void initView(){
        if (rentDetails != null) {
            initRv();
            initPicture(pictures.get(0));
            initMapData();

            tvRentAmount.setText(getString(R.string.rent_amount)+" "+rentDetails.rentAmount);
            tvTitle.setText(getString(R.string.rent_name)+" "+rentDetails.rentTitle);
            tvDetails.setText(getString(R.string.rent_description)+" "+rentDetails.rentDetails);
            tvLocation.setText(getString(R.string.rent_location)+" "+rentDetails.location.place);
            tvType.setText(getString(R.string.rent_type)+" "+rentDetails.apartmentType);


            if (rentDetails.apartmentType.toLowerCase().equals(ValueConstants.RENT_TYPE_FAMILY_NOT_SHARED.toLowerCase())) {
                tvRoomNumber.setText(getString(R.string.rent_room_number)+" "+rentDetails.noOfRoom);
                tvAprtSize.setText(getString(R.string.rent_apartment_szie)+" "+rentDetails.apartmentSize);
            } else if (rentDetails.apartmentType.toLowerCase().equals(ValueConstants.RENT_TYPE_FAMIL_SHARED.toLowerCase())) {
                tvRoomNumber.setText(getString(R.string.rent_room_number)+" "+rentDetails.noOfRoom);
                tvAprtSize.setVisibility(View.GONE);
            } else if (rentDetails.apartmentType.toLowerCase().equals(ValueConstants.RENT_TYPE_OFFICE_SPACE.toLowerCase())) {
                tvAprtSize.setText(getString(R.string.rent_apartment_szie)+" "+rentDetails.apartmentSize);
                tvRoomNumber.setVisibility(View.GONE);
            } else if (rentDetails.apartmentType.toLowerCase().equals(ValueConstants.RENT_TYPE_GARAGE.toLowerCase())) {
                tvRoomNumber.setVisibility(View.GONE);
                tvAprtSize.setVisibility(View.GONE);
            }
        }
    }

    private void initPicture(Picture picture){
        Glide.with(this)
                .load(picture.pictureUrl)
                .into(ivRentPicture);
    }

    private void initRv(){
        rvPictures.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        rvPictures.setHasFixedSize(true);
        rvPictures.setItemAnimator(new DefaultItemAnimator());
        rvPictures.setNestedScrollingEnabled(false);
        rvPictures.addItemDecoration(new DividerItemDecoration(this, LinearLayoutManager.HORIZONTAL));

        PictureRvAdapter adapter = new PictureRvAdapter(this, pictures, new PictureRvAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(Picture picture) {
                initPicture(picture);
            }
        });

        adapter.notifyDataSetChanged();
        rvPictures.setAdapter(adapter);
    }


    @Override
    public boolean onMarkerClick(Marker marker) {
        return false;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }else {
//            mMap.setMyLocationEnabled(true);
        }
    }

    private void initMapData(){
        try {
            LatLng rentLatLng = new LatLng(Double.parseDouble(rentDetails.location.lat), Double.parseDouble(rentDetails.location.lon));
            MarkerOptions markerOptions = new MarkerOptions()
                    .position(rentLatLng)
                    .title(rentDetails.rentTitle);

            mMap.addMarker(markerOptions).setTag(0);

            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(rentLatLng, 15));
        }catch (Exception e){
            e.printStackTrace();
        }

    }

    @OnClick(R.id.btnNav)
    public void initNavigation(){
        Uri gmmIntentUri = Uri.parse("google.navigation:q="+rentDetails.location.lat+","+rentDetails.location.lon);
        Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
        mapIntent.setPackage("com.google.android.apps.maps");
        startActivity(mapIntent);
    }

    @Override
    public void onRentDetailsFetched(RentDetailsModel rentDetails) {
        this.rentDetails = rentDetails;
        pictures = rentDetails.rentPictures.get(0);
        initView();
    }

    @OnClick(R.id.ivFavorite)
    public void favThisRent(){
        ivFavorite.setImageResource(R.drawable.ic_favorite);

    }

    @OnClick(R.id.btnBook)
    public void bookThisRent(){
        btnBook.setText("Booked");
        btnBook.setEnabled(false);
    }
}
