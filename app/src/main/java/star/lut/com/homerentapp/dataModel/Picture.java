package star.lut.com.homerentapp.dataModel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Picture {
    @SerializedName("picture_url")
    @Expose
    public String pictureUrl;

    @Override
    public String toString() {
        return "Picture{" +
                "pictureUrl='" + pictureUrl + '\'' +
                '}';
    }
}
