package star.lut.com.homerentapp.appModule.mother.presenter;

import android.content.Context;

/**
 * Created by kamrulhasan on 17/8/18.
 */
public class MotherPresenter {
    private Context context;
    private MotherViewInterface motherViewInterface;

    public MotherPresenter(Context context, MotherViewInterface motherViewInterface) {
        this.context = context;
        this.motherViewInterface = motherViewInterface;
    }
}
