package star.lut.com.homerentapp.appModule.auth.presenter;

import android.content.Context;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Response;
import star.lut.com.homerentapp.apiService.AuthWebService;
import star.lut.com.homerentapp.apiService.WebServiceFactory;
import star.lut.com.homerentapp.dataModel.sendModel.SmsVerificationSendModel;

/**
 * Created by kamrulhasan on 10/9/18.
 */
public class UserValidationPresenter {
    private Context context;
    private UserValidationViewInterface viewInterface;
    private AuthWebService webService;

    public UserValidationPresenter(Context context, UserValidationViewInterface viewInterface) {
        this.context = context;
        this.viewInterface = viewInterface;
    }

    /**
     * @param smsCode
     * make view to be able to identify if its resetpassword or singup validation
     */

    public void verifyCode(String smsCode, String phone){
        webService = WebServiceFactory.createRetrofitService(AuthWebService.class);
        SmsVerificationSendModel sendModel = new SmsVerificationSendModel();
        sendModel.mobile = phone;
        sendModel.smsCode = smsCode;
        webService.verifySms(sendModel)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<Response<JsonArray>>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(Response<JsonArray> jsonObjectResponse) {
                        if (jsonObjectResponse.isSuccessful()){
                            if (jsonObjectResponse.body().get(0).getAsJsonObject().get("status").getAsBoolean()){
                                viewInterface.onCodeVerified();
                            }else {
                                viewInterface.onCodeInvalid();
                            }
                        }
                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }
}
