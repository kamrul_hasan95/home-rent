package star.lut.com.homerentapp.dataModel.rent;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

import star.lut.com.homerentapp.dataModel.Picture;
import star.lut.com.homerentapp.dataModel.response.Location;


/**
 * Created by kamrulhasan on 21/10/18.
 */
public class RentDetailsModel {
    @SerializedName("no")
    @Expose
    public String no;
    @SerializedName("rent_id")
    @Expose
    public String rentId;
    @SerializedName("rent_title")
    @Expose
    public String rentTitle;
    @SerializedName("rent_details")
    @Expose
    public String rentDetails;
    @SerializedName("location")
    @Expose
    public Location location;
    @SerializedName("apartment_type")
    @Expose
    public String apartmentType;
    @SerializedName("rent_amount")
    @Expose
    public String rentAmount;
    @SerializedName("no_of_room")
    @Expose
    public String noOfRoom;
    @SerializedName("apartment_size")
    @Expose
    public String apartmentSize;
    @SerializedName("booked")
    @Expose
    public String booked;
    @SerializedName("owner_booked")
    @Expose
    public String ownerBooked;
    @SerializedName("nearby_places")
    @Expose
    public String nearbyPlaces;
    @SerializedName("rent_pictures")
    @Expose
    public List<List<Picture>> rentPictures = null;

    @Override
    public String toString() {
        return "RentDetailsModel{" +
                "no='" + no + '\'' +
                ", rentId='" + rentId + '\'' +
                ", rentTitle='" + rentTitle + '\'' +
                ", rentDetails='" + rentDetails + '\'' +
                ", location=" + location +
                ", apartmentType='" + apartmentType + '\'' +
                ", rentAmount='" + rentAmount + '\'' +
                ", noOfRoom='" + noOfRoom + '\'' +
                ", apartmentSize='" + apartmentSize + '\'' +
                ", booked='" + booked + '\'' +
                ", ownerBooked='" + ownerBooked + '\'' +
                ", nearbyPlaces='" + nearbyPlaces + '\'' +
                ", rentPictures=" + rentPictures +
                '}';
    }
}