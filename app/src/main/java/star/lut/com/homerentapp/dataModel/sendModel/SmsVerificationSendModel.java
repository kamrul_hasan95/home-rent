package star.lut.com.homerentapp.dataModel.sendModel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SmsVerificationSendModel {
    @SerializedName("mobile")
    @Expose
    public String mobile;
    @SerializedName("sms_code")
    @Expose
    public String smsCode;
}
