package star.lut.com.homerentapp.appModule.auth.view;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.FragmentManager;
import android.view.View;

import butterknife.BindView;
import butterknife.OnClick;
import star.lut.com.homerentapp.R;
import star.lut.com.homerentapp.appModule.auth.presenter.NewPasswordPresenter;
import star.lut.com.homerentapp.appModule.auth.presenter.NewPasswordViewInterface;
import star.lut.com.homerentapp.base.BaseFragment;

/**
 * Created by kamrulhasan on 10/9/18.
 */
public class NewPasswordFragment extends BaseFragment implements NewPasswordViewInterface{

    private static final String ARG_TITLE = "title";
    private String title;
    private NewPasswordPresenter presenter;

    @BindView(R.id.tilNewPassword) public TextInputLayout tilNewPassword;
    @BindView(R.id.tilNewPasswordAgain) public TextInputLayout tilNewPasswordAgain;

    private String password, passwordAgain;

    @Override
    public int getLayoutId() {
        return R.layout.fragment_new_password;
    }

    public NewPasswordFragment() {
    }

    public static NewPasswordFragment newInstance(String title){
        NewPasswordFragment singUpFragment = new NewPasswordFragment();
        Bundle args = new Bundle();
        args.putString(ARG_TITLE, title);
        singUpFragment.setArguments(args);
        return singUpFragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null)
            title = getArguments().getString(ARG_TITLE);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        presenter = new NewPasswordPresenter(getContext(), this);
    }

    @OnClick(R.id.btnResetPassword)
    public void resetPassword(){
        if (validateNewPassword() | validateNewPasswordAgain()){
            presenter.changePassword(password);
        }
    }


    private boolean validateNewPassword(){
        password = tilNewPassword.getEditText().getText().toString().trim();
        if (password.isEmpty()){
            tilNewPassword.setErrorEnabled(true);
            tilNewPassword.setError("Password"+getString(R.string.null_field));
            return false;
        } else if (password.length() < 8){
            tilNewPassword.setErrorEnabled(true);
            tilNewPassword.setError("Password"+getString(R.string.password_short));
            return false;
        } else {
            tilNewPassword.setErrorEnabled(false);
            tilNewPassword.setError(null);
            return true;
        }
    }

    private boolean validateNewPasswordAgain(){
        passwordAgain = tilNewPasswordAgain.getEditText().getText().toString().trim();
        if (passwordAgain.isEmpty()){
            tilNewPasswordAgain.setErrorEnabled(true);
            tilNewPasswordAgain.setError("Password"+getString(R.string.null_field));
            return false;
        } else if (passwordAgain.length() < 8){
            tilNewPasswordAgain.setErrorEnabled(true);
            tilNewPasswordAgain.setError("Password"+getString(R.string.password_short));
            return false;
        }else if (!passwordAgain.equals(password)){
            tilNewPasswordAgain.setErrorEnabled(true);
            tilNewPasswordAgain.setError(getString(R.string.password_didnt_match));
            return false;
        }else {
            tilNewPasswordAgain.setErrorEnabled(false);
            tilNewPasswordAgain.setError(null);
            return true;
        }
    }

    @Override
    public void onPasswordChagned(boolean passwordChanged) {
        if (passwordChanged){
            LoginFragment userValidationFragment = LoginFragment.newInstance("Login");
            FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
            fragmentManager.beginTransaction()
                    .replace(R.id.fragmentContainer, userValidationFragment)
                    .commit();
        }else {
            //show error message cause password was not chagned
        }
    }
}
