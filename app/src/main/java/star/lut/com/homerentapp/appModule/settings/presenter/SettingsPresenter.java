package star.lut.com.homerentapp.appModule.settings.presenter;

import android.content.Context;

/**
 * Created by kamrulhasan on 19/8/18.
 */
public class SettingsPresenter {
    private Context context;
    private SettingsViewInterface viewInterface;

    public SettingsPresenter(Context context, SettingsViewInterface viewInterface) {
        this.context = context;
        this.viewInterface = viewInterface;
    }
}
