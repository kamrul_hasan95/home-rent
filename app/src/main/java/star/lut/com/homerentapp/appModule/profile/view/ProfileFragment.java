package star.lut.com.homerentapp.appModule.profile.view;

import android.Manifest;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;

import butterknife.BindView;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;
import star.lut.com.homerentapp.R;
import star.lut.com.homerentapp.appModule.profile.presenter.ProfilePresenter;
import star.lut.com.homerentapp.base.BaseApplication;
import star.lut.com.homerentapp.base.BaseFragment;
import star.lut.com.homerentapp.appModule.profile.presenter.ProfileViewInterface;
import star.lut.com.homerentapp.constants.ApiConstants;
import star.lut.com.homerentapp.constants.ValueConstants;
import star.lut.com.homerentapp.dataModel.Picture;
import star.lut.com.homerentapp.db.greenDao.Profile;
import timber.log.Timber;

public class ProfileFragment extends BaseFragment implements ProfileViewInterface {
    private static final String ARG_TITLE = "title";
    private String title;
    private ProfilePresenter presenter;

    @BindView(R.id.tvName) public TextView tvName;
    @BindView(R.id.tvPhone) public TextView tvPhone;
    @BindView(R.id.tvEmail) public TextView tvEmail;
    @BindView(R.id.ivProfilePicture) public CircleImageView ivProfilePicture;

    private Picture picture;
    private Profile profile;

    @Override
    public int getLayoutId() {
        return R.layout.fragment_profile;
    }

    public ProfileFragment() {
    }

    public ProfileFragment newInstance(String title){
        ProfileFragment forgotPasswordFragment = new ProfileFragment();
        Bundle args = new Bundle();
        args.putString(ARG_TITLE, title);
        forgotPasswordFragment.setArguments(args);
        return forgotPasswordFragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null)
            title = getArguments().getString(title);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        presenter = new ProfilePresenter(getContext(), this);
        initData();
    }

    private void initData() {
        profile = ((BaseApplication)getContext().getApplicationContext()).getDaoSession().getProfileDao().loadAll().get(0);

        if (profile != null)
            initView();
    }

    public void initView(){
        tvName.setText(getString(R.string.tv_name)+" "+profile.email);
        tvEmail.setText(getString(R.string.tv_email)+" "+profile.email);
        tvPhone.setText(getString(R.string.tv_phone)+" "+profile.mobileNumber);
        String picpath = ApiConstants.BASE_URL+"upload_picture/"+profile.pic;

        Timber.d("picpath "+""+picpath);

        Glide.with(getContext())
                .load(picpath)
                .into(ivProfilePicture);
    }

    @OnClick(R.id.btnUpdate)
    public void updatebtn(){

    }

    @OnClick(R.id.ivEdit)
    public void editProfilePicture(){
        if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(),
                    Manifest.permission.READ_EXTERNAL_STORAGE)) {
                showExplanation();
            } else {
                requestPermission();
            }
        }else {
            presenter.selectPictures();
        }
    }
    private void requestPermission() {
        ActivityCompat.requestPermissions(getActivity(),
                new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, ValueConstants.RESULT_CODE_FOR_STORAGE_PERMISSION);
    }


    private void showExplanation() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(getString(R.string.storage_permission_title))
                .setMessage(getString(R.string.storage_permission_explaination))
                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        requestPermission();
                    }
                });
        builder.create().show();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case ValueConstants.RESULT_CODE_FOR_STORAGE_PERMISSION:
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    presenter.selectPictures();
                } else {
                    Toast.makeText(getActivity(), getString(R.string.permission_denied), Toast.LENGTH_SHORT).show();
                }
        }
    }


    @Override
    public void onImageSelected(Uri uri) {
        presenter.upLoadPicture(uri, profile.mobileNumber);
        picture = new Picture();
        Glide.with(getContext())
                .load(uri)
                .into(ivProfilePicture);
    }

    @Override
    public void onPictureUploaded(String fileName) {
        profile.pic = fileName;

        ((BaseApplication)getActivity().getApplication()).getDaoSession().getProfileDao().update(profile);
    }
}
