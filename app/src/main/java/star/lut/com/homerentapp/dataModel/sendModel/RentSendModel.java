package star.lut.com.homerentapp.dataModel.sendModel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class RentSendModel {
    @SerializedName("api")
    @Expose
    public String api;
    @SerializedName("title")
    @Expose
    public String title;
    @SerializedName("details")
    @Expose
    public String details;
    @SerializedName("apartid")
    @Expose
    public String apartid;
    @SerializedName("locationid")
    @Expose
    public String locationid;
    @SerializedName("nearby")
    @Expose
    public String nearby;
    @SerializedName("is_booked")
    @Expose
    public String isBooked;
    @SerializedName("is_booked_conf")
    @Expose
    public String isBookedConf;
    @SerializedName("amount")
    @Expose
    public String amount;
    @SerializedName("apart_size")
    @Expose
    public String aprt_size;
    @SerializedName("room_no")
    @Expose
    public String room_no;
}
