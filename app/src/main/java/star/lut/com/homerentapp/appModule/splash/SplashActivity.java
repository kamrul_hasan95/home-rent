package star.lut.com.homerentapp.appModule.splash;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import star.lut.com.homerentapp.appModule.auth.view.AuthActivity;
import star.lut.com.homerentapp.appModule.mother.view.MotherActivity;
import star.lut.com.homerentapp.appModule.rent.view.RentUploadActivity;

public class SplashActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        startActivity(new Intent(SplashActivity.this, MotherActivity.class));
        finish();
    }
}
