package star.lut.com.homerentapp.appModule.auth.presenter;

import android.content.Context;
import android.widget.Toast;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import java.util.List;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Response;
import star.lut.com.homerentapp.apiService.AuthWebService;
import star.lut.com.homerentapp.apiService.WebServiceFactory;
import star.lut.com.homerentapp.dataModel.sendModel.RegistrationSendModel;

/**
 * Created by kamrulhasan on 17/8/18.
 */
public class SingUpPresenter {
    private Context context;
    private SingUpViewInterface singUpViewInterface;
    private AuthWebService webService;

    public SingUpPresenter(Context context, SingUpViewInterface singUpViewInterface) {
        this.context = context;
        this.singUpViewInterface = singUpViewInterface;
    }

    public void registerUser(RegistrationSendModel registrationSendModel){

        webService = WebServiceFactory.createRetrofitService(AuthWebService.class);

        webService.registerUser(registrationSendModel)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<Response<List<JsonObject>>>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(Response<List<JsonObject>> jsonObjectResponse) {

                        if (jsonObjectResponse.isSuccessful()){
                            JsonElement jsonElement = jsonObjectResponse.body().get(0);
                            JsonObject jsonObject = jsonElement.getAsJsonObject();
                            if (jsonObject.get("status").getAsBoolean()){
                                String sms = jsonObject.get("sms").getAsString();
                                singUpViewInterface.onRegistrationSuccessful(sms);
                            }else{
                                if (jsonObject.get("error_code").getAsString().equals("NOT_VERIFIED")){
                                    singUpViewInterface.onRegistrationUnsuccessful(jsonObject.get("sms_code").toString());
                                }else{
                                    singUpViewInterface.onRegistrationUnsuccessful();
                                }
                            }
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }
}
