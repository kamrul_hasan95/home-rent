package star.lut.com.homerentapp.appModule.rent.view;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.TextInputLayout;
import android.transition.TransitionManager;
import android.view.View;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.google.android.gms.maps.model.LatLng;

import butterknife.BindView;
import butterknife.OnClick;
import star.lut.com.homerentapp.R;
import star.lut.com.homerentapp.appModule.rent.presenter.SortingPresenter;
import star.lut.com.homerentapp.base.BaseFragment;
import star.lut.com.homerentapp.appModule.rent.presenter.SortingViewInterface;
import star.lut.com.homerentapp.constants.ValueConstants;

import static android.app.Activity.RESULT_OK;

public class SortingFragment extends BaseFragment implements SortingViewInterface {
    private static final String ARG_TITLE = "title";
    private String title;
    private SortingPresenter presenter;
    private String rent_type;
    private LatLng latLng;

    @BindView(R.id.rootView) public ConstraintLayout rootView;
    @BindView(R.id.tilRentAmountLow) public TextInputLayout tilAmountLow;
    @BindView(R.id.tilRentAmountHigh) public TextInputLayout tilAmountHigh;
    @BindView(R.id.rgRentType) public RadioGroup rgRentType;
    @BindView(R.id.tilApartSizeMin) public TextInputLayout tilApartSizeMin;
    @BindView(R.id.tilApartSizeMax) public TextInputLayout tilApartSizeMax;
    @BindView(R.id.tvAprtSize) public TextView tvAprtSize;
    @BindView(R.id.tvLocation) public TextView tvLocation;

    @Override
    public int getLayoutId() {
        return R.layout.fragment_sort;
    }

    public SortingFragment() {
    }

    public SortingFragment instanceOf(){
        SortingFragment sortingFragment = new SortingFragment();
        Bundle args = new Bundle();
        args.putString(ARG_TITLE, title);
        sortingFragment.setArguments(args);
        return sortingFragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null)
            title = getArguments().getString(title);
    }


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        presenter = new SortingPresenter(getContext(), this);
    }

    public void initView(){
        rent_type = ValueConstants.RENT_TYPE_FAMILY_NOT_SHARED;

        processRentType();
    }

    private void processRentType(){
        rgRentType.setOnCheckedChangeListener((group, checkedId) -> {
            if (checkedId == R.id.rbFlat) {
                TransitionManager.beginDelayedTransition(rootView);
                tilApartSizeMax.setVisibility(View.VISIBLE);
                tilApartSizeMin.setVisibility(View.VISIBLE);
                tvAprtSize.setVisibility(View.VISIBLE);
                rent_type = ValueConstants.RENT_TYPE_FAMILY_NOT_SHARED;
            } else if (checkedId == R.id.rbSharedRoom){
                TransitionManager.beginDelayedTransition(rootView);
                tilApartSizeMax.setVisibility(View.VISIBLE);
                tilApartSizeMin.setVisibility(View.VISIBLE);
                tvAprtSize.setVisibility(View.VISIBLE);
                rent_type = ValueConstants.RENT_TYPE_FAMIL_SHARED;
            } else if (checkedId == R.id.rbOfficeSpace){
                TransitionManager.beginDelayedTransition(rootView);
                tilApartSizeMax.setVisibility(View.VISIBLE);
                tilApartSizeMin.setVisibility(View.VISIBLE);
                tvAprtSize.setVisibility(View.VISIBLE);
                rent_type = ValueConstants.RENT_TYPE_OFFICE_SPACE;
            }else if (checkedId == R.id.rbGarage){
                TransitionManager.beginDelayedTransition(rootView);
                tilApartSizeMax.setVisibility(View.GONE);
                tilApartSizeMin.setVisibility(View.GONE);
                tvAprtSize.setVisibility(View.GONE);
                rent_type = ValueConstants.RENT_TYPE_GARAGE;
            }
        });
    }

    @OnClick(R.id.tvLocation)
    public void getLocation(){

        int PLACE_PICKER_REQUEST = ValueConstants.RESULT_CODE_SORT_LOCATION;
        PlacePicker.IntentBuilder builder = new PlacePicker.IntentBuilder();
        try {
            startActivityForResult(builder.build(getActivity()), PLACE_PICKER_REQUEST);
        } catch (GooglePlayServicesRepairableException e) {
            e.printStackTrace();
        } catch (GooglePlayServicesNotAvailableException e) {
            e.printStackTrace();
        }
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK){
            if (requestCode == ValueConstants.RESULT_CODE_SORT_LOCATION){
                Place place = PlacePicker.getPlace(data, getActivity());
                if (place.getLatLng() != null){
                    latLng = place.getLatLng();
                    tvLocation.setText(place.getAddress());
                }
            }
        }
    }


    @OnClick(R.id.btnSort)
    public void sortResult(){

    }
}
