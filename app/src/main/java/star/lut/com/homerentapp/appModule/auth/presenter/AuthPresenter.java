package star.lut.com.homerentapp.appModule.auth.presenter;

import android.content.Context;
import android.util.Log;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by kamrulhasan on 17/8/18.
 */
public class AuthPresenter {
    private Context context;
    private AuthViewInterface authViewInterface;

    public AuthPresenter(Context context, AuthViewInterface authViewInterface) {
        this.context = context;
        this.authViewInterface = authViewInterface;
    }
}
