package star.lut.com.homerentapp.dataModel.sendModel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by kamrulhasan on 25/10/18.
 */
public class FavRentList {
    @SerializedName("api")
    @Expose
    public String api;
    @SerializedName("rent_id")
    @Expose
    public String rentid;

    public FavRentList(String rentid) {
        this.rentid = rentid;
    }

    public FavRentList() {
    }
}
