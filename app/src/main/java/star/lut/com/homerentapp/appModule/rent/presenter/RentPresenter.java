package star.lut.com.homerentapp.appModule.rent.presenter;

import android.content.Context;

import com.google.gson.Gson;
import com.google.gson.JsonArray;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import star.lut.com.homerentapp.R;
import star.lut.com.homerentapp.apiService.RentWebService;
import star.lut.com.homerentapp.apiService.WebServiceFactory;
import star.lut.com.homerentapp.base.BaseApplication;
import star.lut.com.homerentapp.constants.ApiConstants;
import star.lut.com.homerentapp.dataModel.rent.RentDetailsModel;
import star.lut.com.homerentapp.dataModel.response.RentResponse;
import star.lut.com.homerentapp.dataModel.response.RentResponseMain;
import star.lut.com.homerentapp.dataModel.sendModel.FavRentList;
import star.lut.com.homerentapp.dataModel.sendModel.RentList;
import star.lut.com.homerentapp.db.greenDao.FavRent;
import timber.log.Timber;

/**
 * Created by kamrulhasan on 17/8/18.
 */
public class RentPresenter {
    private Context context;
    private RentViewInterface viewInterface;
    private RentWebService rentWebService;
    private int i = 0;

    public RentPresenter(Context context, RentViewInterface viewInterface) {
        this.context = context;
        this.viewInterface = viewInterface;
    }

    public void getRentList(int no){
        rentWebService = WebServiceFactory.createRetrofitService(RentWebService.class);
        RentList rentList = new RentList();
        rentList.api = ApiConstants.API_GELL_ALL_RENT;
        rentList.no = no;
        rentList.numberOfItem = 5;

        Timber.d(rentList.toString());

        rentWebService.getRentList(rentList)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<List<RentResponse>>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(List<RentResponse> rentResponses) {
                        Timber.d(rentResponses.toString());

                        List<RentDetailsModel> rentDetailsModels = new ArrayList<>();

                        for (int i = 0 ; i < rentResponses.size() ; i++){
                            rentDetailsModels.add(rentResponses.get(0).rentDetails);
                        }

                        if (rentDetailsModels.size() > 0){
                            viewInterface.onNewRentList(rentDetailsModels);
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    public void getFavlist() {


//        List<FavRent> favRents = ((BaseApplication)context.getApplicationContext()).getDaoSession().getFavRentDao().loadAll();
//        rentWebService = WebServiceFactory.createRetrofitService(RentWebService.class);
//
//        List<RentDetailsModel> models = new ArrayList<>();
//
//        for (i = 0 ; i < favRents.size() ; i++){
//            FavRentList rentList = new FavRentList(favRents.get(i).rentId);
//
//            rentWebService.getfavRentList(rentList)
//                    .subscribeOn(Schedulers.io())
//                    .observeOn(AndroidSchedulers.mainThread())
//                    .subscribe(new Observer<RentResponse>() {
//                        @Override
//                        public void onSubscribe(Disposable d) {
//
//                        }
//
//                        @Override
//                        public void onNext(RentResponse rentResponse) {
//                            if (rentResponse.rentDetails != null){
////                                models.addAll(rentResponse.rentDetails);
//                            }
//
//                            if (i == favRents.size() - 1){
////                                viewInterface.onNewRentList(rentResponse);
//                            }
//                        }
//
//                        @Override
//                        public void onError(Throwable e) {
//
//                        }
//
//                        @Override
//                        public void onComplete() {
//
//                        }
//                    });
//        }
    }
}
