package star.lut.com.homerentapp.apiService;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import java.util.List;

import io.reactivex.Observable;
import retrofit2.Response;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import star.lut.com.homerentapp.dataModel.Token;
import star.lut.com.homerentapp.dataModel.sendModel.LoginModel;
import star.lut.com.homerentapp.dataModel.sendModel.RegistrationSendModel;
import star.lut.com.homerentapp.dataModel.sendModel.SmsVerificationSendModel;

/**
 * Created by kamrulhasan on 17/8/18.
 */
public interface AuthWebService {
    //singup user
    @POST("signup.php")
    Observable<Response<List<JsonObject>>> registerUser(
            @Body RegistrationSendModel registrationSendModel
    );

    //sinin user
    @POST("signin.php")
    Observable<Response<List<JsonObject>>> loginUser(
            @Body LoginModel loginModel
    );


    @POST("vrify.php")
    Observable<Response<JsonObject>> verifyToken(
            @Body String token
    );

    @POST("sms_verify.php")
    Observable<Response<JsonArray>> verifySms(
            @Body SmsVerificationSendModel model
            );
}
