package star.lut.com.homerentapp.appModule.auth.view;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.FragmentManager;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.OnClick;
import star.lut.com.homerentapp.R;
import star.lut.com.homerentapp.appModule.auth.presenter.ForgotPasswordPresenter;
import star.lut.com.homerentapp.appModule.auth.presenter.ForgotPasswordViewInterface;
import star.lut.com.homerentapp.base.BaseFragment;

/**
 * Created by kamrulhasan on 17/8/18.
 */
public class ForgotPasswordFragment extends BaseFragment implements ForgotPasswordViewInterface{
    private static final String ARG_TITLE = "title";
    private String title;
    private ForgotPasswordPresenter forgotPasswordPresenter;

    @BindView(R.id.tilEmail) public TextInputLayout tilEmail;

    private String email;

    @Override
    public int getLayoutId() {
        return R.layout.fragment_forgot_password;
    }

    public ForgotPasswordFragment() {
    }

    public ForgotPasswordFragment newInstance(String title){
        ForgotPasswordFragment forgotPasswordFragment = new ForgotPasswordFragment();
        Bundle args = new Bundle();
        args.putString(ARG_TITLE, title);
        forgotPasswordFragment.setArguments(args);
        return forgotPasswordFragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null)
            title = getArguments().getString(title);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        forgotPasswordPresenter = new ForgotPasswordPresenter(getContext(), this);
    }

    @OnClick(R.id.ibBack)
    public void loadLoginFragment(){
        LoginFragment loginFragment = new LoginFragment();
        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
        fragmentManager.beginTransaction()
                .replace(R.id.fragmentContainer, loginFragment)
                .commit();
    }

    @OnClick(R.id.btnVerification)
    public void verification(){
        if (validateEmail()){
            forgotPasswordPresenter.sendVerificationCode(email);
        }
    }

    private boolean validateEmail(){
        email = tilEmail.getEditText().getText().toString().trim();
        if (email.isEmpty()){
            tilEmail.setErrorEnabled(true);
            tilEmail.setError("Email"+getString(R.string.null_field));
            return false;
        }else if (!email.contains("@")){
            tilEmail.setErrorEnabled(true);
            tilEmail.setError("Email"+getString(R.string.email_error));
            return false;
        }
        else {
            tilEmail.setErrorEnabled(false);
            tilEmail.setError(null);
            return true;
        }
    }

    @Override
    public void onVerificationCodeSent(boolean b) {
        UserValidationFragment forgotPasswordFragment = new UserValidationFragment();
        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
        fragmentManager.beginTransaction()
                .replace(R.id.fragmentContainer, forgotPasswordFragment)
                .commit();
    }


}
