package star.lut.com.homerentapp.appModule.rent.presenter;

import android.content.Context;

import java.util.List;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import star.lut.com.homerentapp.apiService.RentWebService;
import star.lut.com.homerentapp.apiService.WebServiceFactory;
import star.lut.com.homerentapp.constants.ApiConstants;
import star.lut.com.homerentapp.dataModel.response.RentResponse;
import star.lut.com.homerentapp.dataModel.sendModel.FavRentList;
import timber.log.Timber;

/**
 * Created by kamrulhasan on 18/8/18.
 */
public class RentDetailsPresenter {
    private Context context;
    private RentDetailsViewInterface viewInterface;
    private RentWebService rentWebService;

    public RentDetailsPresenter(Context context, RentDetailsViewInterface viewInterface) {
        this.context = context;
        this.viewInterface = viewInterface;

        rentWebService = WebServiceFactory.createRetrofitService(RentWebService.class);
    }

    public void getRentDetails(String id){
        Timber.d("asdf"+id);
        FavRentList rentList = new FavRentList();
        rentList.rentid = id;
        rentList.api = ApiConstants.API_GET_ONE_RENT;

        rentWebService.getfavRentList(rentList)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<List<RentResponse>>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(List<RentResponse> rentResponses) {
                        Timber.d(rentResponses.toString());
                        if (rentResponses != null && rentResponses.size() != 0){
                            viewInterface.onRentDetailsFetched(rentResponses.get(0).rentDetails);
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }
}
