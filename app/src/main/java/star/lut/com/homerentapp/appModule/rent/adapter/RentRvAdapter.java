package star.lut.com.homerentapp.appModule.rent.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import star.lut.com.homerentapp.R;
import star.lut.com.homerentapp.base.BaseApplication;
import star.lut.com.homerentapp.constants.ApiConstants;
import star.lut.com.homerentapp.dataModel.rent.RentDetailsModel;
import star.lut.com.homerentapp.db.greenDao.FavRent;
import star.lut.com.homerentapp.db.greenDao.FavRentDao;

/**
 * Created by kamrulhasan on 17/8/18.
 */
public class RentRvAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>{
    private int VIEW_LOADING = 0, VIEW_RENT = 1;
    private List<RentDetailsModel> rentModels;
    private OnItemClickListener onItemClickListener;
    private OnLoadingMore onLoadingMore;
    private Context context;

    public interface OnItemClickListener {
        void onItemClick(RentDetailsModel rentModel, int position);
    }

    public interface OnLoadingMore{
        void onLoadingMode(int size, int no);
    }

    public RentRvAdapter(Context context, List<RentDetailsModel> rentModels, OnItemClickListener onItemClickListener, OnLoadingMore onLoadingMore) {
        this.rentModels = rentModels;
        this.onItemClickListener = onItemClickListener;
        this.onLoadingMore = onLoadingMore;
        this.context = context;
    }

    class ViewHolderRent extends RecyclerView.ViewHolder {
        @BindView(R.id.clRentRV) public ConstraintLayout clRentRv;
        @BindView(R.id.tvRentName) public TextView tvRentName;
        @BindView(R.id.tvRentLocation) public TextView tvRentLocation;
        @BindView(R.id.ivRentPicture) public ImageView ivRentPicture;
        @BindView(R.id.ivFavorite) public ImageView ivFavorite;
        @BindView(R.id.tvRentAmount) public TextView tvRentAmount;

        public ViewHolderRent(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    class ViewHolderLoading extends RecyclerView.ViewHolder {
        @BindView(R.id.progressBar1) public ProgressBar progressBar;

        public ViewHolderLoading(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder vh;
        if (viewType == VIEW_RENT) {
            View v = LayoutInflater.from(parent.getContext()).inflate(
                    R.layout.rv_rent, parent, false);

            vh = new ViewHolderRent(v);
        } else {
            View v = LayoutInflater.from(parent.getContext()).inflate(
                    R.layout.view_loading, parent, false);

            vh = new ViewHolderLoading(v);
        }
        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if (position >= rentModels.size()){
            ((ViewHolderLoading) holder).progressBar.setIndeterminate(true);
            if (rentModels.size() == 0){

            }else {
                onLoadingMore.onLoadingMode(10, Integer.parseInt(rentModels.get(rentModels.size() - 1).no));
            }
        }else {
            setUpRentViewHolder((ViewHolderRent)holder, position);
        }

    }

    private void setUpRentViewHolder(ViewHolderRent holder, int position) {
        holder.tvRentName.setText(rentModels.get(position).rentDetails);
        holder.tvRentLocation.setText(rentModels.get(position).location.place);
        holder.tvRentAmount.setText(rentModels.get(position).rentAmount);
        boolean isFav = false;
        if (((BaseApplication)context.getApplicationContext()).getDaoSession().getFavRentDao().queryBuilder().where(FavRentDao.Properties.RentId.eq(rentModels.get(position).rentId)).list().size() > 0){
            isFav = true;
        }

        if (isFav) {
            holder.ivFavorite.setImageResource(R.drawable.ic_favorite);
        }

        boolean finalIsFav = isFav;
        holder.ivFavorite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (finalIsFav){
                    FavRent favRent = ((BaseApplication)context.getApplicationContext()).getDaoSession().getFavRentDao().queryBuilder().where(FavRentDao.Properties.RentId.eq(rentModels.get(position).rentId)).list().get(0);
                    ((BaseApplication)context.getApplicationContext()).getDaoSession().getFavRentDao().delete(favRent);
                    holder.ivFavorite.setImageResource(R.drawable.ic_favorite_gray);
                }else {
                    FavRent favRent = new FavRent();
                    favRent.rentId = rentModels.get(position).rentId;
                    ((BaseApplication)context.getApplicationContext()).getDaoSession().getFavRentDao().insert(favRent);
                    holder.ivFavorite.setImageResource(R.drawable.ic_favorite);
                }
            }
        });

        holder.clRentRv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onItemClickListener.onItemClick(rentModels.get(position), position);
                setFavorite(rentModels.get(position));
            }
        });

        Glide.with(context)
                .load(ApiConstants.BASE_URL + rentModels.get(position).rentPictures.get(0).get(0).pictureUrl)
                .into(holder.ivRentPicture);
    }

    private void setFavorite(RentDetailsModel rentDetailsModel){
        FavRent favRent = new FavRent();
        favRent.rentId = rentDetailsModel.rentId;

        ((BaseApplication)context.getApplicationContext()).getDaoSession().getFavRentDao().insert(favRent);
    }

    @Override
    public int getItemCount() {
        return rentModels.size()+1;
    }

    @Override
    public int getItemViewType(int position) {
        if (position >= rentModels.size())
            return VIEW_LOADING;
        else
            return VIEW_RENT;
    }
}
