package star.lut.com.homerentapp.appModule.settings.view;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.OnClick;
import star.lut.com.homerentapp.R;
import star.lut.com.homerentapp.appModule.settings.presenter.SettingsPresenter;
import star.lut.com.homerentapp.appModule.settings.presenter.SettingsViewInterface;
import star.lut.com.homerentapp.base.BaseActivity;

public class SettingsActivity extends BaseActivity implements SettingsViewInterface {

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.tvFragmentTitle)
    TextView tvFragmentTitle;

    @BindView(R.id.fragmentContainer) public FrameLayout fragmentContainer;

    private SettingsPresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setSupportActionBar(toolbar);
        tvFragmentTitle.setText(getString(R.string.menu_settings));

        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null){
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setHomeAsUpIndicator(R.drawable.ic_arrow_back_black_24px);
        }

        presenter = new SettingsPresenter(this, this);
    }

    @Override
    public int getLayout() {
        return R.layout.activity_settings;
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return super.onSupportNavigateUp();
    }

    @OnClick(R.id.btnSettings)
    public void goToSettings(){
        fragmentContainer.setVisibility(View.VISIBLE);

        Fragment fragment = new SettingsFragment();

        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.fragmentContainer, fragment);
        transaction.commit();
    }
}
