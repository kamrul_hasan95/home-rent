package star.lut.com.homerentapp.db;

import android.content.Context;
import android.content.SharedPreferences;

import star.lut.com.homerentapp.constants.PreferenceConstants;

/**
 * Created by kamrulhasan on 21/10/18.
 */
public class PreferenceManager {
    private Context mContext;

    public PreferenceManager(Context context) {
        this.mContext = context;
    }

    //get shared pref
    private SharedPreferences getPreferences() {
        return mContext.getSharedPreferences(PreferenceConstants.PREFERENCE_NAME, Context.MODE_PRIVATE);
    }

    public void clearPref(){
        getPreferences().edit().clear().apply();
    }

    //set if user is logged in or not
    public void setUserLoggedIn(boolean isLoggedin){
        getPreferences().edit().putBoolean(
                PreferenceConstants.USER_LOGGED_IN, isLoggedin
        ).apply();
    }

    //get if user is logged in or not
    public boolean getUserLoggedIn(){
        return getPreferences().getBoolean(
                PreferenceConstants.USER_LOGGED_IN, false
        );
    }

    //set user notification settings
    public void setNotification(boolean isNotiOn){
        getPreferences().edit().putBoolean(
                PreferenceConstants.NOTIFICATION_SETTINGS, isNotiOn
        ).apply();
    }

    //get if notification is on or not
    public boolean getNotification(){
        return getPreferences().getBoolean(
                PreferenceConstants.NOTIFICATION_SETTINGS, true
        );
    }

    //set if app language english
    public void setLanguage(boolean isEnglish){
        getPreferences().edit().putBoolean(
                PreferenceConstants.LANGUAGE_ENGLISH, isEnglish
        ).apply();
    }

    //get if app language is english. true = english, false = bengali
    public boolean getLanguage(){
        return getPreferences().getBoolean(
                PreferenceConstants.LANGUAGE_ENGLISH, true
        );
    }

    //set user accessstoken for future usage
    public void setToken(String token){
        getPreferences().edit().putString(
                PreferenceConstants.ACCESS_TOKEN, token
        ).apply();
    }

    //get user accessToken for future usage
    public String getToken(){
        return getPreferences().getString(
                PreferenceConstants.ACCESS_TOKEN, "token"
        );
    }

    public void setUsername(String userName){
        getPreferences().edit().putString(
                PreferenceConstants.USER_NAME, userName
        ).apply();
    }

    public String getUserName(){
        return getPreferences().getString(
                PreferenceConstants.USER_NAME, "a"
        );
    }

    public void setEmail(String email){
        getPreferences().edit().putString(
                PreferenceConstants.USER_EMAIL, email
        ).apply();
    }

    public String getEmail(){
        return getPreferences().getString(
                PreferenceConstants.USER_EMAIL, "a"
        );
    }

    public void setUserPhone(String phone){
        getPreferences().edit().putString(
                PreferenceConstants.USER_PHONE, phone
        ).apply();
    }

    public String getUserPhone(){
        return getPreferences().getString(
                PreferenceConstants.USER_PHONE, "a"
        );
    }


    public void setUserPic(String pic){
        getPreferences().edit().putString(
                PreferenceConstants.USER_PIC, pic
        ).apply();
    }

    public String getUserPic(){
        return getPreferences().getString(
                PreferenceConstants.USER_PIC, "a"
        );
    }
}
