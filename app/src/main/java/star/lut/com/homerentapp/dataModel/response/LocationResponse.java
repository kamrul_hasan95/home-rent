package star.lut.com.homerentapp.dataModel.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class LocationResponse {
    @SerializedName("status")
    @Expose
    public String status;
    @SerializedName("msg")
    @Expose
    public String msg;
    @SerializedName("update")
    @Expose
    public String update;
    @SerializedName("id")
    @Expose
    public String id;

    @Override
    public String toString() {
        return "LocationResponse{" +
                "status='" + status + '\'' +
                ", msg='" + msg + '\'' +
                ", update='" + update + '\'' +
                ", id='" + id + '\'' +
                '}';
    }
}
