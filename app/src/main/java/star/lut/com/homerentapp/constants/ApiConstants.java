package star.lut.com.homerentapp.constants;

/**
 * Created by kamrulhasan on 17/8/18.
 */
public interface ApiConstants {

    //API BASE url
    String BASE_URL = "https://stupidarnob.com/phpAPI/"; // live base url
    String HEADER_NAME_CONTENT = "Content-Type";
    String CONTENT_TYPE_JSON = "application/json";
    String CONTENT_TYPE_MULTIPART = "multipart/form-data";


    String API_INSERT_RENT = "insert";
    String API_INSERT_LOCATION = "location";
    String API_GELL_ALL_RENT = "searchQ1";
    String API_GET_ONE_RENT = "searchQ2";
}
