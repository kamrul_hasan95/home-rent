package star.lut.com.homerentapp.dataModel.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import star.lut.com.homerentapp.dataModel.Profile;


/**
 * Created by kamrulhasan on 21/10/18.
 */
public class LoginResponseModel {
    @SerializedName("status")
    @Expose
    public Boolean status;
    @SerializedName("msg")
    @Expose
    public String msg;
    @SerializedName("user_profile")
    @Expose
    public Profile profile;
}
