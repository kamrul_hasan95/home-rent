package star.lut.com.homerentapp.appModule.profile.presenter;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.support.v4.content.ContextCompat;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import java.io.File;
import java.util.ArrayList;

import gun0912.tedbottompicker.TedBottomPicker;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Response;
import star.lut.com.homerentapp.R;
import star.lut.com.homerentapp.apiService.UploadWebService;
import star.lut.com.homerentapp.apiService.WebServiceFactory;
import star.lut.com.homerentapp.appModule.mother.view.MotherActivity;
import timber.log.Timber;

public class ProfilePresenter {
    private Context context;
    private ProfileViewInterface viewInterface;

    public ProfilePresenter(Context context, ProfileViewInterface viewInterface) {
        this.context = context;
        this.viewInterface = viewInterface;
    }

    public void selectPictures(){
        if (ContextCompat.checkSelfPermission(context, Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED){
            TedBottomPicker tedBottomPicker = new TedBottomPicker.Builder(context)
                    .setOnImageSelectedListener(new TedBottomPicker.OnImageSelectedListener() {
                        @Override
                        public void onImageSelected(Uri uri) {
                            viewInterface.onImageSelected(uri);
                        }
                    })
                    .showTitle(true)
                    .setTitle(context.getString(R.string.rent_upload_upload))
                    .setCompleteButtonText(context.getString(R.string.done))
                    .create();

            MotherActivity activity = (MotherActivity) context;
            tedBottomPicker.show(activity.getSupportFragmentManager());
        }
    }

    public void upLoadPicture(Uri uri, String mobile) {
        File file = new File(uri.getPath());
        String filename = file.getName();

        Timber.d(filename+"    "+mobile);

        RequestBody fbody = RequestBody.create(MediaType.parse("image/*"), file);
        RequestBody fileName = RequestBody.create(MediaType.parse("text/plain"), filename);
        RequestBody where = RequestBody.create(MediaType.parse("text/plain"), "user");
        RequestBody mobile1 = RequestBody.create(MediaType.parse("text/plain"), mobile);
        RequestBody aprt_id = RequestBody.create(MediaType.parse("text/plain"),"");

        UploadWebService webService = WebServiceFactory.createRetrofitService(UploadWebService.class);

        webService.uploadProfilePicture(fbody, fileName, where, mobile1, aprt_id)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<Response<JsonObject>>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(Response<JsonObject> jsonElementResponse) {
                        Timber.d(jsonElementResponse.body().toString());
                        viewInterface.onPictureUploaded(filename);
                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }
}
