package star.lut.com.homerentapp.db.greenDao;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Generated;
import org.greenrobot.greendao.annotation.Id;

@Entity(nameInDb = "favorite_rents")
public class FavRent {
    @Id(autoincrement = true)
    private Long id;
    @SerializedName("rent_id")
    @Expose
    public String rentId;
    @Generated(hash = 1051202877)
    public FavRent(Long id, String rentId) {
        this.id = id;
        this.rentId = rentId;
    }
    @Generated(hash = 460484686)
    public FavRent() {
    }
    public Long getId() {
        return this.id;
    }
    public void setId(Long id) {
        this.id = id;
    }
    public String getRentId() {
        return this.rentId;
    }
    public void setRentId(String rentId) {
        this.rentId = rentId;
    }
}
