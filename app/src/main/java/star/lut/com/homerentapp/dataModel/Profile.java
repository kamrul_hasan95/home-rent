package star.lut.com.homerentapp.dataModel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by kamrulhasan on 4/11/18.
 */
public class Profile {
    @SerializedName("name")
    @Expose
    public String name;
    @SerializedName("id")
    @Expose
    public String backId;
    @SerializedName("email")
    @Expose
    public String email;
    @SerializedName("pic")
    @Expose
    public String pic;
    @SerializedName("token")
    @Expose
    public String token;
    @SerializedName("mobile_number")
    @Expose
    public String mobileNumber;

    @Override
    public String toString() {
        return "Profile{" +
                "name='" + name + '\'' +
                ", backId='" + backId + '\'' +
                ", email='" + email + '\'' +
                ", pic='" + pic + '\'' +
                ", token='" + token + '\'' +
                ", mobileNumber='" + mobileNumber + '\'' +
                '}';
    }
}
