package star.lut.com.homerentapp.dataModel.sendModel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by kamrulhasan on 21/10/18.
 */
public class LoginModel {
    @SerializedName("username")
    @Expose
    public String username;
    @SerializedName("password")
    @Expose
    public String password;

    public LoginModel(String username, String password) {
        this.username = username;
        this.password = password;
    }
}
