package star.lut.com.homerentapp.appModule.rent.presenter;

import android.net.Uri;

import java.util.List;

/**
 * Created by kamrulhasan on 20/8/18.
 */
public interface RentUploadViewInterface {
    void onRentLocationUploaded(String locationId);
    void onRentDetailsUploaded(String rentID);

    void onPictureSelected(List<Uri> pictures);

    void onPictureUploaded();
}
