package star.lut.com.homerentapp.appModule.auth.presenter;

import android.content.Context;

/**
 * Created by kamrulhasan on 10/9/18.
 */
public class NewPasswordPresenter {
    private Context context;
    private NewPasswordViewInterface viewInterface;

    public NewPasswordPresenter(Context context, NewPasswordViewInterface viewInterface) {
        this.context = context;
        this.viewInterface = viewInterface;
    }

    public void changePassword(String password){
        viewInterface.onPasswordChagned(true);
    }
}
