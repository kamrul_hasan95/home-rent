package star.lut.com.homerentapp.dataModel.sendModel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class RentLocationSendModel {
    @SerializedName("api")
    @Expose
    public String api;
    @SerializedName("lat")
    @Expose
    public double lat;
    @SerializedName("long")
    @Expose
    public double lon;
    @SerializedName("location_name")
    @Expose
    public String locationName;
}
