package star.lut.com.homerentapp.dataModel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by kamrulhasan on 21/10/18.
 */
public class Token {
    @SerializedName("iat")
    @Expose
    public Integer iat;
    @SerializedName("jti")
    @Expose
    public String jti;
    @SerializedName("exp")
    @Expose
    public Integer exp;

    public Token(Integer iat, String jti, Integer exp) {
        this.iat = iat;
        this.jti = jti;
        this.exp = exp;
    }
}
