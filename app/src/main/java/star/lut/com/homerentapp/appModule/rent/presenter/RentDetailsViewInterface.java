package star.lut.com.homerentapp.appModule.rent.presenter;


import star.lut.com.homerentapp.dataModel.rent.RentDetailsModel;
import star.lut.com.homerentapp.dataModel.response.RentResponse;

/**
 * Created by kamrulhasan on 18/8/18.
 */
public interface RentDetailsViewInterface {
    void onRentDetailsFetched(RentDetailsModel rentDetails);
}
