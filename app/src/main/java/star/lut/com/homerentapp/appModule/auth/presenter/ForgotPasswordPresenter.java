package star.lut.com.homerentapp.appModule.auth.presenter;

import android.content.Context;

import star.lut.com.homerentapp.apiService.AuthWebService;

/**
 * Created by kamrulhasan on 17/8/18.
 */
public class ForgotPasswordPresenter {
    private Context context;
    private ForgotPasswordViewInterface forgotPasswordViewInterface;
    private AuthWebService authWebService;

    public ForgotPasswordPresenter(Context context, ForgotPasswordViewInterface forgotPasswordViewInterface) {
        this.context = context;
        this.forgotPasswordViewInterface = forgotPasswordViewInterface;
    }

    public void sendVerificationCode(String email){
        forgotPasswordViewInterface.onVerificationCodeSent(true);
    }
}
