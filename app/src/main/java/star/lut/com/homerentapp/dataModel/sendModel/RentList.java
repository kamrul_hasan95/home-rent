package star.lut.com.homerentapp.dataModel.sendModel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by kamrulhasan on 21/10/18.
 */
public class RentList {
    @SerializedName("api")
    @Expose
    public String api;
    @SerializedName("no")
    @Expose
    public int no;
    @SerializedName("number_of_item")
    @Expose
    public int numberOfItem;

    @Override
    public String toString() {
        return "RentList{" +
                "api='" + api + '\'' +
                ", no=" + no +
                ", numberOfItem=" + numberOfItem +
                '}';
    }
}
