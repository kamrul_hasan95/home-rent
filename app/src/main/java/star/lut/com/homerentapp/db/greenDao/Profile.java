package star.lut.com.homerentapp.db.greenDao;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.Generated;

/**
 * Created by kamrulhasan on 22/10/18.
 */
@Entity(nameInDb = "profile")
public class Profile {
    @Id(autoincrement = false)
    public Long id;

    @SerializedName("name")
    @Expose
    public String name;
    @SerializedName("id")
    @Expose
    public String backId;
    @SerializedName("email")
    @Expose
    public String email;
    @SerializedName("pic")
    @Expose
    public String pic;
    @SerializedName("token")
    @Expose
    public String token;
    @SerializedName("mobile_number")
    @Expose
    public String mobileNumber;
    @Generated(hash = 973547840)
    public Profile(Long id, String name, String backId, String email, String pic,
            String token, String mobileNumber) {
        this.id = id;
        this.name = name;
        this.backId = backId;
        this.email = email;
        this.pic = pic;
        this.token = token;
        this.mobileNumber = mobileNumber;
    }
    @Generated(hash = 782787822)
    public Profile() {
    }
    public Long getId() {
        return this.id;
    }
    public void setId(Long id) {
        this.id = id;
    }
    public String getName() {
        return this.name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public String getBackId() {
        return this.backId;
    }
    public void setBackId(String backId) {
        this.backId = backId;
    }
    public String getEmail() {
        return this.email;
    }
    public void setEmail(String email) {
        this.email = email;
    }
    public String getPic() {
        return this.pic;
    }
    public void setPic(String pic) {
        this.pic = pic;
    }
    public String getToken() {
        return this.token;
    }
    public void setToken(String token) {
        this.token = token;
    }
    public String getMobileNumber() {
        return this.mobileNumber;
    }
    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }
}
