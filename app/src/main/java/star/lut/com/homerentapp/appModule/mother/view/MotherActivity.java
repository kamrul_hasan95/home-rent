package star.lut.com.homerentapp.appModule.mother.view;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.os.Bundle;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.ittianyu.bottomnavigationviewex.BottomNavigationViewEx;

import butterknife.BindView;
import star.lut.com.homerentapp.R;
import star.lut.com.homerentapp.appModule.auth.view.AuthActivity;
import star.lut.com.homerentapp.appModule.auth.view.LoginFragment;
import star.lut.com.homerentapp.appModule.exchange.view.ExchangeFragment;
import star.lut.com.homerentapp.appModule.mother.presenter.MotherPresenter;
import star.lut.com.homerentapp.appModule.mother.presenter.MotherViewInterface;
import star.lut.com.homerentapp.appModule.profile.view.ProfileFragment;
import star.lut.com.homerentapp.appModule.rent.view.RentFragment;
import star.lut.com.homerentapp.appModule.rent.view.RentUploadActivity;
import star.lut.com.homerentapp.appModule.rent.view.SortingFragment;
import star.lut.com.homerentapp.appModule.settings.view.SettingsActivity;
import star.lut.com.homerentapp.base.BaseActivity;
import star.lut.com.homerentapp.base.BaseApplication;
import star.lut.com.homerentapp.db.PreferenceManager;

public class MotherActivity extends BaseActivity implements MotherViewInterface, NavigationView.OnNavigationItemSelectedListener {
    @BindView(R.id.bottomNavigationView)
    BottomNavigationViewEx bottomNavigationView;
    @BindView(R.id.fragmentContainer)
    FrameLayout fragmentContainer;

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.drawer_layout)
    DrawerLayout drawerLayout;
    @BindView(R.id.nav_view)
    NavigationView navigationView;

    @BindView(R.id.tvFragmentTitle)
    TextView tvFragmentTitle;

    private FragmentManager fragmentManager;
    private Fragment fragment;
    private boolean showFragmentAgain;
    private PreferenceManager preferenceManager;
    private Intent authIntent;

    @Override
    public int getLayout() {
        return R.layout.activity_mother;
    }

    private MotherPresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        presenter = new MotherPresenter(this, this);

        preferenceManager = new PreferenceManager(this);
        authIntent = new Intent(this, AuthActivity.class);
        initView();
    }

    private void initView() {

        setSupportActionBar(toolbar);
        //toolbar.setTitle("");
        if (getSupportActionBar() != null)
            getSupportActionBar().setDisplayShowTitleEnabled(false);

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawerLayout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawerLayout.addDrawerListener(toggle);
        toggle.syncState();

        Menu menu = navigationView.getMenu();
        MenuItem menuItem = menu.findItem(R.id.menu_nav_login);

        if (preferenceManager.getUserLoggedIn()) {
            menuItem.setTitle(R.string.logout_text);
        } else {
            menuItem.setTitle(R.string.login_text);
        }

        navigationView.setNavigationItemSelectedListener(this);

        customizeTabLayout(bottomNavigationView);

        fragmentManager = getSupportFragmentManager();
        fragment = new RentFragment();
        loadFragment(fragment, getResources().getString(R.string.menu_rent));

        bottomNavigationView.setItemIconTintList(null);

        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.menu_bottom_rent:
                        fragment = new RentFragment();
                        loadFragment(fragment, getString(R.string.menu_rent));
                        navigationView.getMenu().getItem(0).setChecked(false);
                        return true;
                    case R.id.menu_bottom_sort:
                        if (!(fragment instanceof SortingFragment)) {
                            fragment = new SortingFragment();
                            loadFragment(fragment, getResources().getString(R.string.menu_sort));
                        } else if (showFragmentAgain) {
                            fragment = new SortingFragment();
                            loadFragment(fragment, getResources().getString(R.string.menu_sort));
                            showFragmentAgain = false;
                        }
                        return true;
                    case R.id.menu_bottom_change:
                        if (!(fragment instanceof ExchangeFragment)) {
                            fragment = new ExchangeFragment();
                            loadFragment(fragment, getResources().getString(R.string.menu_change));
                        } else if (showFragmentAgain) {
                            fragment = new ExchangeFragment();
                            loadFragment(fragment, getResources().getString(R.string.menu_change));
                            showFragmentAgain = false;
                        }
                        navigationView.getMenu().getItem(1).setChecked(true);
                        return true;
                    case R.id.menu_bottom_profile:
                        if (preferenceManager.getUserLoggedIn()) {
                            if (!(fragment instanceof ProfileFragment)) {
                                fragment = new ProfileFragment();
                                loadFragment(fragment, getResources().getString(R.string.menu_profile));
                            } else if (showFragmentAgain) {
                                fragment = new ProfileFragment();
                                loadFragment(fragment, getResources().getString(R.string.menu_profile));
                                showFragmentAgain = false;
                            }
                            return true;
                        } else {
                            login();
                        }

                }
                return false;
            }
        });
    }

    private void loadFragment(Fragment fragment, String fragmentTitle) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.fragmentContainer, fragment);
        transaction.commit();

        tvFragmentTitle.setText(fragmentTitle);
    }

    private void customizeTabLayout(BottomNavigationViewEx bnve) {
        bnve.enableAnimation(false);
        bnve.enableShiftingMode(false);
        bnve.enableItemShiftingMode(false);
        bnve.setTextVisibility(false);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();


        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.menu_nav_favorite) {
            fragment = new RentFragment();
            ((RentFragment) fragment).setFavScreen(true);
            bottomNavigationView.setSelectedItemId(R.id.menu_bottom_rent);
            loadFragment(fragment, getResources().getString(R.string.menu_fav));
        } else if (id == R.id.menu_nav_exchange) {
            fragment = new ExchangeFragment();
            bottomNavigationView.setSelectedItemId(R.id.menu_bottom_change);
            loadFragment(fragment, getResources().getString(R.string.menu_change));
        } else if (id == R.id.menu_nav_post_ad) {
            if (preferenceManager.getUserLoggedIn()) {
                ContextCompat.startActivity(this, new Intent(this, RentUploadActivity.class), ActivityOptionsCompat.makeClipRevealAnimation(View.inflate(this, R.layout.fragment_exchange, null), 10, 10, 20, 20).toBundle());
            } else {
                login();
            }
        } else if (id == R.id.menu_nav_settings) {
            ContextCompat.startActivity(this, new Intent(this, SettingsActivity.class), ActivityOptionsCompat.makeClipRevealAnimation(View.inflate(this, R.layout.fragment_exchange, null), 10, 10, 20, 20).toBundle());
        } else if (id == R.id.menu_nav_login) {
            if (preferenceManager.getUserLoggedIn()) {
                logout();
            } else {
                login();
            }
        } else if (id == R.id.menu_nav_about_us) {

        } else if (id == R.id.menu_nav_share) {

        } else if (id == R.id.menu_nav_rate) {

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void login() {
        startActivity(authIntent);
        finish();
    }

    private void logout() {
        preferenceManager.clearPref();
        ((BaseApplication) getApplication()).getDaoSession().clear();
        startActivity(new Intent(MotherActivity.this, MotherActivity.class));
        finish();
    }


}
