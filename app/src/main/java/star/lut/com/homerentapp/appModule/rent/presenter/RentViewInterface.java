package star.lut.com.homerentapp.appModule.rent.presenter;

import java.util.List;

import star.lut.com.homerentapp.dataModel.rent.RentDetailsModel;
import star.lut.com.homerentapp.dataModel.response.RentResponse;

/**
 * Created by kamrulhasan on 17/8/18.
 */
public interface RentViewInterface {
    void onNewRentList(List<RentDetailsModel> rentResponse);
}
