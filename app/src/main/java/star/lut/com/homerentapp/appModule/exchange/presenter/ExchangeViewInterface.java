package star.lut.com.homerentapp.appModule.exchange.presenter;

import com.google.android.gms.maps.model.LatLng;

import java.util.List;

public interface ExchangeViewInterface {

    void onDirectionFetched(List<LatLng> latLngs);
}
