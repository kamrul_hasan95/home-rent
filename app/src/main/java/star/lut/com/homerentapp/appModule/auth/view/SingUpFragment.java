package star.lut.com.homerentapp.appModule.auth.view;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.OnClick;
import star.lut.com.homerentapp.R;
import star.lut.com.homerentapp.appModule.auth.presenter.SingUpPresenter;
import star.lut.com.homerentapp.appModule.auth.presenter.SingUpViewInterface;
import star.lut.com.homerentapp.base.BaseFragment;
import star.lut.com.homerentapp.dataModel.sendModel.RegistrationSendModel;

/**
 * Created by kamrulhasan on 17/8/18.
 */
public class SingUpFragment extends BaseFragment implements SingUpViewInterface {
    private static final String ARG_TITLE = "title";
    private String title;
    private SingUpPresenter singUpPresenter;

    @BindView(R.id.tvLogin) public TextView tvLogin;
    @BindView(R.id.tilUserName) public TextInputLayout etUsername;
    @BindView(R.id.tilPhone) public TextInputLayout etPhone;
    @BindView(R.id.tilFullName) public TextInputLayout etFullName;
    @BindView(R.id.tilPassword) public TextInputLayout etPassword;
    @BindView(R.id.tilEmail) public TextInputLayout etEmail;

    private String fullName, phone, username, password, email;

    @Override
    public int getLayoutId() {
        return R.layout.fragment_singup;
    }

    public SingUpFragment() {
    }

    public SingUpFragment newInstance(String title){
        SingUpFragment singUpFragment = new SingUpFragment();
        Bundle args = new Bundle();
        args.putString(ARG_TITLE, title);
        singUpFragment.setArguments(args);
        return singUpFragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null)
            title = getArguments().getString(ARG_TITLE);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        singUpPresenter = new SingUpPresenter(getContext(), this);

        makeLoginTextClickable();
    }

    private void makeLoginTextClickable(){
        tvLogin.setText(getString(R.string.login_text));

        SpannableString ss = new SpannableString(getString(R.string.already_have_account)+"\n"+getString(R.string.login_text));
        ClickableSpan clickableSpan = new ClickableSpan() {
            @Override
            public void onClick(View textView) {
                LoginFragment fragmentLogin = new LoginFragment();
                FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                fragmentManager.beginTransaction()
                        .replace(R.id.fragmentContainer, fragmentLogin)
                        .commit();
            }

            @Override
            public void updateDrawState(TextPaint ds) {
                super.updateDrawState(ds);
                ds.setUnderlineText(false);
            }
        };
        ss.setSpan(clickableSpan, 0, ss.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

        tvLogin.setText(ss);
        tvLogin.setMovementMethod(LinkMovementMethod.getInstance());
        tvLogin.setHighlightColor(getResources().getColor(R.color.colorPrimary));
    }

    @OnClick(R.id.btnSingup)
    public void singUp(){
        if (validateFullName() | validatePassword() | validatePhone() | validateUserName() | validateEmail()){
            RegistrationSendModel registrationSendModel = new RegistrationSendModel();
            registrationSendModel.username = username;
            registrationSendModel.password = password;
            registrationSendModel.fullName = fullName;
            registrationSendModel.email = email;
            registrationSendModel.mobile = phone;

            singUpPresenter.registerUser(registrationSendModel);
        }else{
            phone = null;
            fullName = null;
            password = null;
            username = null;
            email = null;
        }
    }

    private boolean validateEmail(){
        email = etEmail.getEditText().getText().toString().trim();
        if (fullName == null || fullName.length() == 0){
            etEmail.setErrorEnabled(true);
            etEmail.setError(getString(R.string.et_email)+getString(R.string.null_field));
            return false;
        }else if (!email.contains("@")){
            etEmail.setErrorEnabled(true);
            etEmail.setError(getString(R.string.et_email)+getString(R.string.email_error));
            return false;
        } else {
            etFullName.setErrorEnabled(false);
            etFullName.setError(null);
            return true;
        }
    }

    private boolean validateFullName(){
        fullName = etFullName.getEditText().getText().toString().trim();
        if (fullName == null || fullName.length() == 0){
            etFullName.setErrorEnabled(true);
            etFullName.setError(getString(R.string.et_fullname)+getString(R.string.null_field));
            return false;
        }
        else {
            etFullName.setErrorEnabled(false);
            etFullName.setError(null);
            return true;
        }
    }

    private boolean validateUserName(){
        username = etUsername.getEditText().getText().toString().trim();
        if (username == null || username.length() == 0){
            etUsername.setErrorEnabled(true);
            etUsername.setError(getString(R.string.et_username)+getString(R.string.null_field));
            return false;
        }else if(username.length() > 15){
            etUsername.setErrorEnabled(true);
            etUsername.setError(getString(R.string.username_long));
            return false;
        }
        else {
            etUsername.setErrorEnabled(false);
            etUsername.setError(null);
            return true;
        }
    }


    private boolean validatePhone(){
        phone = etPhone.getEditText().getText().toString().trim();
        if (phone.isEmpty()){
            etPhone.setErrorEnabled(true);
            etPhone.setError("Phone"+getString(R.string.null_field));
            return false;
        }else if (phone.length() != 11){
            etPhone.setErrorEnabled(true);
            etPhone.setError(getString(R.string.phone_number_error));
            return false;
        } else {
            etPhone.setErrorEnabled(false);
            etPhone.setError(null);
            return true;
        }
    }


    private boolean validatePassword(){
        password = etPassword.getEditText().getText().toString().trim();
        if (password.isEmpty()){
            etPassword.setErrorEnabled(true);
            etPassword.setError("Password"+getString(R.string.null_field));
            return false;
        } else if (password.length() < 8){
            etPassword.setErrorEnabled(true);
            etPassword.setError("Password"+getString(R.string.password_short));
            return false;
        } else {
            etPassword.setErrorEnabled(false);
            etPassword.setError(null);
            return true;
        }
    }

    @Override
    public void onRegistrationSuccessful(String code) {
        Toast.makeText(getContext(), "Registration Sucessful! Please verify sms code now." , Toast.LENGTH_SHORT).show();
        authenticate(code);
    }

    @Override
    public void onRegistrationUnsuccessful(String code) {
        Toast.makeText(getContext(), "User Already Exists! Please verify sms code now." , Toast.LENGTH_SHORT).show();
        authenticate(code);
    }

    private void authenticate(String code){
        UserValidationFragment userValidationFragment = UserValidationFragment.newInstance("User Validation");
        userValidationFragment.setData(code, phone);
        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
        fragmentManager.beginTransaction()
                .replace(R.id.fragmentContainer, userValidationFragment)
                .commit();
    }

    @Override
    public void onRegistrationUnsuccessful() {
        Toast.makeText(getContext(), "User Already Exists! Please Login" , Toast.LENGTH_SHORT).show();
    }
}
