package star.lut.com.homerentapp.appModule.rent.presenter;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.support.v4.content.ContextCompat;

import com.google.gson.JsonElement;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import gun0912.tedbottompicker.TedBottomPicker;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import okhttp3.FormBody;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Response;
import retrofit2.http.Multipart;
import retrofit2.http.Part;
import star.lut.com.homerentapp.R;
import star.lut.com.homerentapp.apiService.RentWebService;
import star.lut.com.homerentapp.apiService.UploadWebService;
import star.lut.com.homerentapp.apiService.WebServiceFactory;
import star.lut.com.homerentapp.appModule.rent.view.RentUploadActivity;
import star.lut.com.homerentapp.dataModel.response.LocationResponse;
import star.lut.com.homerentapp.dataModel.response.RentUploadResponse;
import star.lut.com.homerentapp.dataModel.sendModel.RentLocationSendModel;
import star.lut.com.homerentapp.dataModel.sendModel.RentSendModel;

/**
 * Created by kamrulhasan on 20/8/18.
 */
public class RentUploadPresenter {
    private Context context;
    private RentUploadViewInterface viewInterface;
    private RentWebService rentWebService;

    public RentUploadPresenter(Context context, RentUploadViewInterface viewInterface) {
        this.context = context;
        this.viewInterface = viewInterface;
    }

    public void uploadRentDetails(RentSendModel model) {
        rentWebService = WebServiceFactory.createRetrofitService(RentWebService.class);

        rentWebService.setRentDetails(model)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<Response<List<RentUploadResponse>>>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(Response<List<RentUploadResponse>> listResponse) {
                        if (listResponse.isSuccessful()) {
                            viewInterface.onRentDetailsUploaded(listResponse.body().get(0).rent_id);
                        }
                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    public void uploadLocation(RentLocationSendModel model) {
        rentWebService = WebServiceFactory.createRetrofitService(RentWebService.class);

        rentWebService.setRentLocation(model)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<Response<List<LocationResponse>>>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(Response<List<LocationResponse>> listResponse) {
                        if (listResponse.isSuccessful())
                            viewInterface.onRentLocationUploaded(listResponse.body().get(0).id);
                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    public void selectPictures() {
        if (ContextCompat.checkSelfPermission(context, Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
            TedBottomPicker tedBottomPicker = new TedBottomPicker.Builder(context)
                    .setOnMultiImageSelectedListener(new TedBottomPicker.OnMultiImageSelectedListener() {
                        @Override
                        public void onImagesSelected(ArrayList<Uri> uriList) {
                            viewInterface.onPictureSelected(uriList);
                        }
                    })
                    .showTitle(true)
                    .setTitle(context.getString(R.string.rent_upload_upload))
                    .setCompleteButtonText(context.getString(R.string.done))
                    .create();

            RentUploadActivity activity = (RentUploadActivity) context;
            tedBottomPicker.show(activity.getSupportFragmentManager());
        }
    }


    public void uploadPicture(File files, String rentId) {
        RequestBody reqFile = RequestBody.create(MediaType.parse("image/*"), files);
        MultipartBody.Part body =
                MultipartBody.Part.createFormData("user_photo", files.getName(), reqFile);

        RequestBody requestBody = new FormBody.Builder()
                .add("file", body.toString())
                .add("name", files.getName())
                .add("mobile", "")
                .add("where", "aprt")
                .add("aprt_id", rentId)
                .build();

        UploadWebService webService = WebServiceFactory.createRetrofitService(UploadWebService.class);
        webService.rentPicture(requestBody)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<Response<JsonElement>>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(Response<JsonElement> jsonElementResponse) {
                        if (jsonElementResponse.isSuccessful()){
                            viewInterface.onPictureUploaded();
                        }
                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }
}
