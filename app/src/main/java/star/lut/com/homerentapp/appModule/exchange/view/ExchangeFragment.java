package star.lut.com.homerentapp.appModule.exchange.view;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CustomCap;
import com.google.android.gms.maps.model.Dash;
import com.google.android.gms.maps.model.Dot;
import com.google.android.gms.maps.model.Gap;
import com.google.android.gms.maps.model.JointType;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.PatternItem;
import com.google.android.gms.maps.model.Polygon;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.android.gms.maps.model.RoundCap;

import java.util.Arrays;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import star.lut.com.homerentapp.R;
import star.lut.com.homerentapp.appModule.exchange.presenter.ExchangePresenter;
import star.lut.com.homerentapp.appModule.exchange.presenter.ExchangeViewInterface;
import star.lut.com.homerentapp.base.BaseActivity;
import star.lut.com.homerentapp.base.BaseFragment;
import star.lut.com.homerentapp.constants.ValueConstants;
import timber.log.Timber;

import static android.app.Activity.RESULT_OK;

public class ExchangeFragment extends BaseFragment implements ExchangeViewInterface,
        OnMapReadyCallback,
        GoogleMap.OnPolylineClickListener,
        GoogleMap.OnPolygonClickListener  {


    private static final String ARG_TITLE = "title";
    private String title;

    @BindView(R.id.btnCalculate) public Button button;
    @BindView(R.id.tvFrom) public TextView tvFrom;
    @BindView(R.id.tvTo) public TextView tvTO;
    @BindView(R.id.btnGetQuate) public Button btnGetQuate;


    private ExchangePresenter presenter;
    private GoogleMap mMap;

    private LatLng fromLatlng;
    private LatLng toLatlng;


    private static final int COLOR_BLACK_ARGB = 0xff000000;
    private static final int COLOR_WHITE_ARGB = 0xffffffff;
    private static final int COLOR_GREEN_ARGB = 0xff388E3C;
    private static final int COLOR_PURPLE_ARGB = 0xff81C784;
    private static final int COLOR_ORANGE_ARGB = 0xffF57F17;
    private static final int COLOR_BLUE_ARGB = 0xffF9A825;

    private static final int POLYLINE_STROKE_WIDTH_PX = 12;
    private static final int POLYGON_STROKE_WIDTH_PX = 8;
    private static final int PATTERN_DASH_LENGTH_PX = 20;
    private static final int PATTERN_GAP_LENGTH_PX = 20;
    private static final PatternItem DOT = new Dot();
    private static final PatternItem DASH = new Dash(PATTERN_DASH_LENGTH_PX);
    private static final PatternItem GAP = new Gap(PATTERN_GAP_LENGTH_PX);

    // Create a stroke pattern of a gap followed by a dot.
    private static final List<PatternItem> PATTERN_POLYLINE_DOTTED = Arrays.asList(GAP, DOT);

    // Create a stroke pattern of a gap followed by a dash.
    private static final List<PatternItem> PATTERN_POLYGON_ALPHA = Arrays.asList(GAP, DASH);

    // Create a stroke pattern of a dot followed by a gap, a dash, and another gap.
    private static final List<PatternItem> PATTERN_POLYGON_BETA =
            Arrays.asList(DOT, GAP, DASH, GAP);


    @Override
    public int getLayoutId() {
        return R.layout.fragment_exchange;
    }

    public ExchangeFragment() {
    }

    public ExchangeFragment newInstance(String title){
        ExchangeFragment forgotPasswordFragment = new ExchangeFragment();
        Bundle args = new Bundle();
        args.putString(ARG_TITLE, title);
        forgotPasswordFragment.setArguments(args);
        return forgotPasswordFragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null)
            title = getArguments().getString(title);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        presenter = new ExchangePresenter(getContext(), this);

        SupportMapFragment mapFragment = (SupportMapFragment) this.getChildFragmentManager()
                .findFragmentById(R.id.mapFragmentExchange);

        mapFragment.getMapAsync(this);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }else {
            mMap.setMyLocationEnabled(true);
            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(23.8103, 90.4125), 14));
        }
    }

    @OnClick(R.id.tvFrom)
    public void fromSelection(){
        int status = GooglePlayServicesUtil.isGooglePlayServicesAvailable(getActivity());
        if (status != ConnectionResult.SUCCESS){
            Timber.d("available googleplay");
        }else {
            Timber.d("available googleplay not");
        }
        int PLACE_PICKER_REQUEST = ValueConstants.RESULT_CODE_FROM_SEARCH;
        PlacePicker.IntentBuilder builder = new PlacePicker.IntentBuilder();
        try {
            startActivityForResult(builder.build(getActivity()), PLACE_PICKER_REQUEST);
        } catch (GooglePlayServicesRepairableException e) {
            e.printStackTrace();
        } catch (GooglePlayServicesNotAvailableException e) {
            e.printStackTrace();
        }
    }

    @OnClick(R.id.tvTo)
    public void toSelection(){
        int PLACE_PICKER_REQUEST = ValueConstants.RESULT_CODE_TO_SEARCH;
        PlacePicker.IntentBuilder builder = new PlacePicker.IntentBuilder();

        try {
            startActivityForResult(builder.build(getActivity()), PLACE_PICKER_REQUEST);
        } catch (GooglePlayServicesRepairableException e) {
            e.printStackTrace();
        } catch (GooglePlayServicesNotAvailableException e) {
            e.printStackTrace();
        }
    }

    @OnClick(R.id.btnCalculate)
    public void calculate(){
        if (toLatlng != null && fromLatlng != null){
            button.setVisibility(View.VISIBLE);
            if (mMap != null){
                presenter.getLatLong(fromLatlng, toLatlng);
            }
        }else {
            button.setVisibility(View.GONE);
        }
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK){
            if (requestCode == ValueConstants.RESULT_CODE_FROM_SEARCH){
                Place place = PlacePicker.getPlace(data, getActivity());
                if (place.getLatLng() != null){
                    fromLatlng = place.getLatLng();
                    tvFrom.setText(place.getAddress());
                }
            }else if (requestCode == ValueConstants.RESULT_CODE_TO_SEARCH){
                Place place = PlacePicker.getPlace(data, getActivity());
                if (place.getLatLng() != null){
                    toLatlng = place.getLatLng();
                    tvTO.setText(place.getAddress());
                }
            }
        }
    }

    /**
     * Styles the polyline, based on type.
     * @param polyline The polyline object that needs styling.
     */
    private void stylePolyline(Polyline polyline) {
        polyline.setEndCap(new RoundCap());
        polyline.setWidth(POLYLINE_STROKE_WIDTH_PX);
        polyline.setColor(COLOR_BLACK_ARGB);
        polyline.setJointType(JointType.ROUND);
    }

    /**
     * Styles the polygon, based on type.
     * @param polygon The polygon object that needs styling.
     */

    @Override
    public void onPolygonClick(Polygon polygon) {

    }

    @Override
    public void onPolylineClick(Polyline polyline) {

    }

    @Override
    public void onDirectionFetched(List<LatLng> latLngs) {
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(fromLatlng, 12));
        setPolyLine(latLngs);
    }

    private void setPolyLine(List<LatLng> latLngs) {
        if (latLngs != null) {
            btnGetQuate.setVisibility(View.VISIBLE);

            PolylineOptions polylineOptions = new PolylineOptions();
            Timber.d("asd " + latLngs.toString());
            polylineOptions.addAll(latLngs);
            polylineOptions.width(15.0f).color(Color.RED).jointType(JointType.ROUND).endCap(new RoundCap());

            mMap.addPolyline(polylineOptions);
        } else {
            Toast.makeText(getContext(), getString(R.string.quata_limit_text), Toast.LENGTH_LONG).show();
        }
    }

    @OnClick(R.id.btnGetQuate)
    public void getQuata(){

    }
}
