package star.lut.com.homerentapp.dataModel.rent;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by kamrulhasan on 21/10/18.
 */
public class RentLocation {
    @SerializedName("lat")
    @Expose
    public String lat;
    @SerializedName("lon")
    @Expose
    public String lon;
    @SerializedName("place")
    @Expose
    public String place;
}
