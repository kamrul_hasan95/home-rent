package star.lut.com.homerentapp.appModule.rent.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import star.lut.com.homerentapp.R;
import star.lut.com.homerentapp.dataModel.Picture;

/**
 * Created by kamrulhasan on 24/10/18.
 */
public class UploadedPictureRvAdapter extends RecyclerView.Adapter<UploadedPictureRvAdapter.ViewHolder> {
    private Context context;
    private List<Picture> pictures;
    private final OnItemClickListener listener;

    public interface OnItemClickListener {
        void onItemClick(Picture picture);
    }

    public UploadedPictureRvAdapter(Context context, List<Picture> pictures, OnItemClickListener listener) {
        this.context = context;
        this.pictures = pictures;
        this.listener = listener;
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        @BindView(R.id.ivPicture) public ImageView ivPicture;
        @BindView(R.id.ivRemove) public ImageView ivRemove;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);


        }

        public void initView(Picture picture){
            Glide.with(itemView.getContext())
                    .load(picture.pictureUrl)
                    .into(ivPicture);

            ivPicture.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onItemClick(picture);
                }
            });
        }
    }

    @NonNull
    @Override
    public UploadedPictureRvAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(context)
                .inflate(R.layout.rv_upload_picture, parent, false);
        return new UploadedPictureRvAdapter.ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull UploadedPictureRvAdapter.ViewHolder holder, int position) {
        holder.initView(pictures.get(position));
        holder.ivRemove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pictures.remove(holder.getAdapterPosition());
                notifyDataSetChanged();
            }
        });

    }

    @Override
    public int getItemCount() {
        return pictures.size();
    }

}
